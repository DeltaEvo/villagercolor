package xyz.deltaevo.villagercolor.game.window;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import xyz.deltaevo.villagercolor.game.Game;
import xyz.deltaevo.villagercolor.game.GameManager;
import xyz.deltaevo.villagercolor.window.InventoryCancelableCloseEvent;
import xyz.deltaevo.villagercolor.window.Window;

import java.util.ResourceBundle;

public class GameSelectWindow extends Window{
    private final GameManager manager;
    private Game.GameStateChange listener;

    /**
     * Create new Arena Select Window
     * @param bundle bundle to get Arena name
     * @param manager GameManager to get games
     */
    public GameSelectWindow(ResourceBundle bundle , GameManager manager) {
        super(ChatColor.translateAlternateColorCodes('&' , bundle.getString("arenaSelectWindowName"))
                , getSize(manager.getGames().size()), true);
        this.manager = manager;
        this.listener = game -> build();
        build();
    }

    /**
     * Build window
     */
    protected void build(){
        int i = 0;
        for(Game game : manager.getGames()){
            game.addListener(listener);
            if(game.isStarted())
                continue;

            addItem(i, newIS(Material.COMPASS, game.getPlayers().size() , game.getArena().getName()), e -> {
                e.getView().close();
                game.addPlayer((Player) e.getWhoClicked());
            });
            i++;
            //I could handle this
            if(i == 54)
                return;
        }
    }

    /**
     * Get a 9 multiple size in [0 ; 54]
     * @param elements elements to get size
     * @return 9 multiple size in [0 ; 54]
     */
    public static int getSize(int elements){
        int i = elements%9*9;
        return i >= 54 ? 54 : i;
    }


    /**
     * On inventory close
     * @param event bukkit event
     */
    @Override
    public void onInventoryClose(InventoryCancelableCloseEvent event) {
        for(Game game : manager.getGames()){
            game.removeListener(listener);
        }
        super.onInventoryClose(event);
    }
}
