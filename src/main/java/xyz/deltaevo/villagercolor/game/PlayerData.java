package xyz.deltaevo.villagercolor.game;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import xyz.deltaevo.villagercolor.arena.Path;
import xyz.deltaevo.villagercolor.entity.GameVillager;

public class PlayerData {
    private Player player;
    private ItemStack[] inventory;
    private Location lastLocation;
    private int totalExperience;
    private Path path;
    private int points;
    private GameVillager lastVillager;


    /**
     * Create new player data for specified player
     * @param player player to create data
     */
    public PlayerData(Player player) {
        this.player = player;
        this.inventory = player.getInventory().getContents();
        this.lastLocation = player.getLocation();
        this.totalExperience = player.getTotalExperience();
    }

    /**
     * Get player holding this data
     * @return player holding this data
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * Get player's inventory before game
     * @return player's inventory before game
     */
    public ItemStack[] getInventory() {
        return inventory;
    }

    /**
     * Get player's last location before game
     * @return player's last location before game
     */
    public Location getLastLocation() {
        return lastLocation;
    }

    /**
     * Get player's last total experience before game
     * @return player's last total experience before game
     */
    public int getTotalExperience() {
        return totalExperience;
    }

    /**
     * Set current player villager path
     * @param path villager path
     */
    public void setPath(Path path) {
        this.path = path;
    }

    /**
     * Get current player villager path
     * @return current player villager path
     */
    public Path getPath() {
        return path;
    }

    /**
     * Set player's points
     * @param points points to set
     */
    public void setPoints(int points) {
        this.points = points;
    }

    /**
     * Get player's points
     * @return player's points
     */
    public int getPoints() {
        return points;
    }

    /**
     * Get last villager spawned to the path
     * @return last villager spawned to the path
     */
    public GameVillager getLastVillager() {
        return lastVillager;
    }

    /**
     * Set last villager spawned to the path
     * @param lastVillager last villager spawned to the path
     */
    public void setLastVillager(GameVillager lastVillager) {
        this.lastVillager = lastVillager;
    }
}
