package xyz.deltaevo.villagercolor.game;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import xyz.deltaevo.villagercolor.VillagerColor;
import xyz.deltaevo.villagercolor.arena.Arena;
import xyz.deltaevo.villagercolor.arena.ArenaManager;
import xyz.deltaevo.villagercolor.game.window.GameSelectWindow;
import xyz.deltaevo.villagercolor.window.WindowView;

import java.util.Collection;
import java.util.HashSet;

public class GameManager implements Listener{
    private final VillagerColor instance;
    private Collection<Game> games;

    /**
     * Create new GameManager using current VillagerColor instance
     * @param instance VillagerColor instance
     */
    public GameManager(VillagerColor instance) {
        this.games = new HashSet<>();
        this.instance = instance;
        instance.getArenaManager().getArenas().forEach(this::createGame);
        instance.getArenaManager().addListener(new ArenaManager.ArenaListener() {
            @Override
            public void onAdd(Arena arena) {
                createGame(arena);
            }

            @Override
            public void onRemove(Arena arena) {
                return;
            }
        });
    }

    /**
     * Get Game by Arena
     * @param arena arena to get Game
     * @return Game found or null
     */
    public Game getByArena(Arena arena){
        for(Game game : games)
            if(game.getArena().equals(arena))
                return game;
        return null;
    }

    /**
     * Create new game for arena
     * @param arena arena to create game for
     */
    private void createGame(Arena arena){
        games.add(new Game(arena , this));
    }

    /**
     * Get current games
     * @return current games
     */
    public Collection<Game> getGames() {
        return games;
    }

    /**
     * Get VillagerColor instance (used by Game)
     * @return VillagerColor instance
     */
    VillagerColor getInstance() {
        return instance;
    }

    /**
     * Open Game Select Window for player
     * @param p player to open window
     */
    public void openWindowFor(Player p){
        new WindowView(new GameSelectWindow(instance.getBundleManager()
                .getBundle(instance.getLocaleProvider().getLocale(p))
                , this) , p).open();
    }

    /**
     * Call onPlayerInteract on games
     * @param event bukkit event
     */
    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event){
        games.forEach(game -> game.onPlayerInteract(event));
    }

    /**
     * Call onPlayerQuit on games
     * @param event bukkit event
     */
    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event){
        games.forEach(game -> game.onPlayerQuit(event));
    }
}
