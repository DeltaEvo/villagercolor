package xyz.deltaevo.villagercolor.game;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Door;
import org.bukkit.material.Gate;
import org.bukkit.material.TrapDoor;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import xyz.deltaevo.villagercolor.VillagerColor;
import xyz.deltaevo.villagercolor.arena.Arena;
import xyz.deltaevo.villagercolor.arena.Path;
import xyz.deltaevo.villagercolor.entity.GameVillager;
import xyz.deltaevo.villagercolor.entity.GameVillagerColor;
import xyz.deltaevo.villagercolor.i18n.Variables;
import xyz.deltaevo.villagercolor.java.Container;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class Game{

    //All door material
    private static final Collection<Material> DOORS = Arrays.asList(Material.ACACIA_DOOR ,
            Material.DARK_OAK_DOOR , Material.BIRCH_DOOR , Material.IRON_DOOR , Material.JUNGLE_DOOR,
            Material.SPRUCE_DOOR , Material.WOOD_DOOR);

    //All fences material
    private static final Collection<Material> FENCES = Arrays.asList(Material.FENCE_GATE ,
            Material.ACACIA_FENCE_GATE , Material.BIRCH_FENCE_GATE , Material.DARK_OAK_FENCE_GATE ,
             Material.JUNGLE_FENCE_GATE , Material.SPRUCE_FENCE_GATE);

    public static final ItemStack QUIT = new ItemStack(Material.BARRIER);

    private Arena arena;
    private GameManager manager;
    private Collection<PlayerData> players;
    private Collection<GameStateChange> listeners;
    private ScheduledFuture<?> wait;
    private ScheduledFuture<?> check;
    private int spawnSchedulerId;
    private Collection<GameVillager> villagers;
    private Scoreboard scoreboard;
    private Objective objective;

    private boolean started;

    /**
     * Create new Game using specified Arena and manager
     * @param arena
     * @param manager
     */
    public Game(Arena arena , GameManager manager) {
        this.arena = arena;
        this.manager = manager;
        this.players = new ArrayList<>(this.arena.getMinPlayers());
        this.listeners = new HashSet<>();
        this.villagers = new CopyOnWriteArrayList<>();
        this.scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        this.objective = this.scoreboard.registerNewObjective(arena.getName() , "dummy");
        this.objective.setDisplaySlot(DisplaySlot.SIDEBAR);
    }

    /**
     * Get current Game Arena
     * @return game arena
     */
    public Arena getArena() {
        return arena;
    }

    /**
     * Get unmodifiable list of players
     * @return unmodifiable list of players
     */
    public Collection<PlayerData> getPlayers() {
        return Collections.unmodifiableCollection(players);
    }


    /**
     * Add player to this game
     * @param player player to add
     */
    public void addPlayer(Player player){
        players.add(new PlayerData(player));

        player.getInventory().setContents(new ItemStack[player.getInventory().getSize()]);
        player.getInventory().setItem(8 , QUIT);

        player.teleport(arena.getLobby());

        player.setScoreboard(scoreboard);
        objective.getScore(player.getName()).setScore(0);
        if(players.size() == arena.getMinPlayers()){
            players.stream().map(PlayerData::getPlayer).forEach(p -> p.setExp(0));
            VillagerColor instance = manager.getInstance();
            Container<Integer> time = new Container<>(instance.getWaitTime());
            wait = instance.getThreadPool().scheduleAtFixedRate(() ->{
                players.stream().map(PlayerData::getPlayer).forEach(p -> p.setLevel(time.getValue()));
                if(time.getValue() == 0){
                    wait.cancel(false);
                    start();
                }
                time.setValue(time.getValue()-1);
            },1, 1 , TimeUnit.SECONDS);
        }else if(players.size() == arena.getPaths().size()){
            if(wait != null)
                wait.cancel(false);
            start();
        }

    }

    /**
     * Remove player in this game
     * @param player player to remove
     */
    public void removePlayer(Player player){
        Iterator<PlayerData> it = players.iterator();
        while(it.hasNext()){
            PlayerData data = it.next();
            Player p = data.getPlayer();
            if(p == player){
                it.remove();
                p.getInventory().setContents(data.getInventory());
                p.updateInventory();
                p.teleport(data.getLastLocation());
                p.setTotalExperience(data.getTotalExperience());
                scoreboard.resetScores(player.getName());
                p.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
                break;
            }
        }
        if(wait != null && players.size() < arena.getMinPlayers())
            wait.cancel(false);
        if(players.isEmpty() && isStarted())
            stop(null);
    }

    /**
     * Get if game is started
     * @return if game is started
     */
    public boolean isStarted() {
        return started;
    }

    /**
     * Set game state
     * @param started if it's started
     */
    private void setStarted(boolean started){
        listeners.forEach(l -> l.onGameStateChange(this));
        this.started = started;
    }

    /**
     * Start game
     */
    protected void start(){
        setStarted(true);
        Iterator<PlayerData> it1 = players.iterator();
        Iterator<Path> it2 = arena.getPaths().iterator();
        while(it1.hasNext()){
            PlayerData data = it1.next();
            Path path = it2.next();
            data.setPath(path);
            data.getPlayer().teleport(path.getPlayerLocation());
        }

        spawnSchedulerId = Bukkit.getScheduler().scheduleSyncRepeatingTask
                (manager.getInstance() , this::spawnVillager , 40 , 40);
        check = manager.getInstance().getThreadPool().scheduleAtFixedRate(this::checkPoints , 50 , 50 , TimeUnit.MILLISECONDS);

    }

    /**
     * Spawn a villager
     */
    protected void spawnVillager(){
        for(PlayerData data : players){
            if(data.getLastVillager() != null)
                data.getLastVillager().goTo(data.getPath().getTo() , 0.5);
            GameVillager villager = manager.getInstance().getFactory().create(data.getPath().getFrom().clone().add(0 , 1 , 0));
            villager.setColor(Math.random() < manager.getInstance().getRedSpawnChance() ? GameVillagerColor.PURPLE : GameVillagerColor.WHITE);
            data.setLastVillager(villager);
            villagers.add(villager);
        }
    }

    /**
     * Stop game
     * @param winner winner of this game
     */
    protected void stop(PlayerData winner){
        check.cancel(true);
        Bukkit.getScheduler().cancelTask(spawnSchedulerId);
        villagers.forEach(GameVillager::remove);
        players.stream().map(PlayerData::getPlayer).forEach(p -> {
            if(winner != null){
                ResourceBundle bundle =
                        manager.getInstance().getBundleManager().getBundle(
                                manager.getInstance().getLocaleProvider().getLocale(p));
                p.sendMessage(bundle.getString("win").replace(Variables.NAME , winner.getPlayer().getName()));
            }
            removePlayer(p);
        });
        setStarted(false);

    }

    /**
     * Check villagers position , to check points
     */
    protected void checkPoints(){
        for(PlayerData data : players){
            Path p = data.getPath();
            for(GameVillager villager : villagers){

                //Check by distance
                if(villager.getLocation().distanceSquared(p.getTo()) >= manager
                        .getInstance().getSquaredDistanceCheck())
                    continue;

                boolean opened = isOpened(p.getTo().getBlock());

                switch (villager.getColor()){
                    case PURPLE:
                            addPoint(data , opened ? -1 : 0);
                        break;
                    case WHITE:
                            addPoint(data , opened ? 1 : 0);
                        break;
                    default:break;
                }
                villager.remove();
                //Concurrent List
                villagers.remove(villager);
            }

            if(data.getPoints() >= manager.getInstance().getPointToWin()){
                stop(data);
                return;
            }
        }
    }

    /**
     * Add points to player
     * @param data  player's data
     * @param points points to add
     */
    private void addPoint(PlayerData data , int points){
        data.setPoints(data.getPoints()+points);
        objective.getScore(data.getPlayer().getName()).setScore(data.getPoints());
    }

    /**
     * Add GameStateChange listener
     * @param listener listener to add
     */
    public void addListener(GameStateChange listener){
        listeners.add(listener);
    }

    /**
     * Add GameStateChange listener
     * @param listener listener to add
     */
    public void removeListener(GameStateChange listener) {
        listeners.remove(listener);
    }

    /**
     * On player interact
     * @param event bukkit event
     */
    void onPlayerInteract(PlayerInteractEvent event){
        if(event.hasItem() && event.getItem().equals(QUIT)) {
            Optional<Player> player = players.stream().map(PlayerData::getPlayer).filter(p -> p == event.getPlayer()).findFirst();
            if(player.isPresent()){
                removePlayer(player.get());
                event.setCancelled(true);
            }
        }
    }

    /**
     * On player quit
     * @param event bukkit event
     */
    void onPlayerQuit(PlayerQuitEvent event){
        removePlayer(event.getPlayer());
    }

    @FunctionalInterface
    public interface GameStateChange{
        /**
         * On game state change (start-stop)
         * @param game game with state have changed
         */
        void onGameStateChange(Game game);
    }

    /**
     * Check if Material is door
     * @param material material to check
     * @return true if it's a door
     */
    public static boolean isDoor(Material material){
        return DOORS.contains(material);
    }

    /**
     * Check if Material is fence
     * @param material material to check
     * @return true if it's a fence
     */
    public static boolean isFence(Material material){
        return FENCES.contains(material);
    }

    /**
     * Check if Material is trapDoor
     * @param material material to check
     * @return true if it's a trapDoor
     */
    public static boolean isTrapDoor(Material material){
        return material == Material.TRAP_DOOR || material == Material.IRON_TRAPDOOR;
    }


    /**
     * Get if a door , fence or trapdoor is opened
     * @param block block to check
     * @return if it's opened
     */
    public static boolean isOpened(Block block){
        if(isDoor(block.getType())){
            Door door = (Door) block.getState().getData();
            return door.isOpen();
        } else if(isFence(block.getType())){
            Gate gate = (Gate) block.getState().getData();
            return gate.isOpen();
        } else if(isTrapDoor(block.getType())){
            TrapDoor trapDoor = (TrapDoor) block.getState().getData();
            return trapDoor.isOpen();
        } else return false;
    }
}
