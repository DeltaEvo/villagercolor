package xyz.deltaevo.villagercolor;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.bukkit.Location;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.plugin.java.JavaPlugin;
import xyz.deltaevo.villagercolor.area.Area;
import xyz.deltaevo.villagercolor.area.PlayerArea;
import xyz.deltaevo.villagercolor.area.cuboid.CuboidAreaCreator;
import xyz.deltaevo.villagercolor.arena.ArenaManager;
import xyz.deltaevo.villagercolor.command.CommandRegisterer;
import xyz.deltaevo.villagercolor.entity.GameVillager;
import xyz.deltaevo.villagercolor.entity.GameVillagerFactory;
import xyz.deltaevo.villagercolor.entity.GameVillagerFactoryRegistry;
import xyz.deltaevo.villagercolor.entity.bukkit.VillagerContainer;
import xyz.deltaevo.villagercolor.entity.nms.NMSVillager;
import xyz.deltaevo.villagercolor.game.GameManager;
import xyz.deltaevo.villagercolor.gson.ConfigurationSerializableAdapter;
import xyz.deltaevo.villagercolor.gson.InterfaceAdapter;
import xyz.deltaevo.villagercolor.i18n.BundleManager;
import xyz.deltaevo.villagercolor.i18n.LocaleProvider;
import xyz.deltaevo.villagercolor.i18n.SpigotLocaleProvider;
import xyz.deltaevo.villagercolor.java.ByteStreams;
import xyz.deltaevo.villagercolor.selector.Selector;
import xyz.deltaevo.villagercolor.window.WindowListener;

import java.io.*;
import java.net.MalformedURLException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;

public class VillagerColor extends JavaPlugin{
    /*
    NMS Package
    */
    public static final String NMS_PACKAGE = "net.minecraft.server";
    /*
        Craftbukkit Package
     */
    public static final String CRAFTBUKKIT_PACKAGE = "org.bukkit.craftbukkit";

    public static final Logger logger = Logger.getLogger(VillagerColor.class.getName());

    public static final String RESOURCE_BUNDLE_BASE_NAME = "messages";

    public static final String ARENA_FOLDER_NAME = "arenas";

    public static final String LABEL = "[VillagerColor]";

    //Current GameVillagerFactory
    private GameVillagerFactory factory;

    private GameVillagerFactoryRegistry factoryRegistry;
    //Current BundleManager
    private BundleManager bundleManager;

    private CommandRegisterer commands;

    private Gson gson;

    private ArenaManager arenaManager;

    private PlayerArea playerAreaManager;

    private LocaleProvider localeProvider;

    private Selector.SelectorListener selectorListener;

    private GameManager gameManager;

    private ScheduledExecutorService threadPool;

    private int waitTime;

    private int pointToWin;

    private double squaredDistanceCheck;

    private double redSpawnChance;

    @Override
    public void onEnable() {
        File messageFolder = new File(getDataFolder() , "lang");
        messageFolder.mkdirs();
        try {
            this.bundleManager = new BundleManager(RESOURCE_BUNDLE_BASE_NAME, messageFolder.toURI().toURL());
        } catch (MalformedURLException e) {
            disablePlugin(messageFolder.getPath() + "Folder could not be transformed to URL" , e);
        }

        File messages = new File(messageFolder , RESOURCE_BUNDLE_BASE_NAME + ".properties");

        if(!messages.exists()){
            try {
                InputStream in = VillagerColor.class.getClassLoader().getResourceAsStream("messages.properties");
                OutputStream out = new FileOutputStream(messages);
                ByteStreams.copy(in , out);
                out.close();
                in.close();
            } catch (IOException e) {
                logger.log(Level.WARNING, "Could't create message.properties file" , e);
            }
        }

        saveDefaultConfig();

        setLocaleProvider(new SpigotLocaleProvider());

        this.gson = new GsonBuilder()
                .enableComplexMapKeySerialization()
                .registerTypeHierarchyAdapter(ConfigurationSerializable.class , ConfigurationSerializableAdapter.INSTANCE)
                .registerTypeAdapter(Area.class , new InterfaceAdapter<Area>())
                .setPrettyPrinting()
                .create();

        this.selectorListener = new Selector.SelectorListener(this);

        getServer().getPluginManager().registerEvents(selectorListener , this);

        this.arenaManager = new ArenaManager(new File(getDataFolder() , ARENA_FOLDER_NAME) , gson);
        arenaManager.loadArenas();

        this.waitTime = getConfig().getInt("waitTime");
        logger.log(Level.INFO , LABEL + " Wait time: " + waitTime);
        this.pointToWin = getConfig().getInt("pointToWin");
        logger.log(Level.INFO , LABEL + " Point To Win: " + pointToWin);
        this.squaredDistanceCheck = getConfig().getDouble("squaredDistanceCheck");
        logger.log(Level.INFO , LABEL + " Squared Distance Check: " + squaredDistanceCheck);
        this.redSpawnChance = getConfig().getDouble("redSpawnChance");
        logger.log(Level.INFO , LABEL + " Red Spawn Chance: " + redSpawnChance);

        this.gameManager = new GameManager(this);
        getServer().getPluginManager().registerEvents(gameManager , this);

        getServer().getPluginManager().registerEvents(new WindowListener(this) ,this);

        this.commands = CommandRegisterer.register(getCommand("villagercolor") , new VillagerColorCommands(this) , "");

        this.playerAreaManager = new PlayerArea(commands , this);

        registerPlayerAreaCreators(playerAreaManager);

        this.factoryRegistry = new GameVillagerFactoryRegistry();
        factoryRegistry.register("nms" , NMSVillager::spawn);
        factoryRegistry.register("bukkit" , VillagerContainer::new);

        this.threadPool = Executors.newScheduledThreadPool(10);

        //On server start end
        getServer().getScheduler().scheduleSyncDelayedTask(this , () ->{
            String backend = getConfig().getString("backend");
            Function<Location , GameVillager> function = factoryRegistry.getFunction(backend);
            if(function == null){
                disablePlugin(LABEL + "Unknown Backend " + backend , new IllegalArgumentException("Could't find backend " + backend));
            }
            else{
                logger.log(Level.INFO , LABEL + " Backend: " + backend);
                VillagerColor.this.factory = new GameVillagerFactory(function);
            }
                }, 1);

    }

    @Override
    public void onDisable() {

        arenaManager.unloadArenas();

        try {
            this.bundleManager.close();
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Could not close ClassLoader", e);
        }
    }

    private void registerPlayerAreaCreators(PlayerArea playerAreaManager) {
        playerAreaManager.registerCreatorCommand(new CuboidAreaCreator.CuboidAreaCreatorCommand());
    }

    /**
     * Disable plugin and log exception
     * @param context context of the error
     * @param error error occurred
     * @see Logger#log(Level, String, Throwable)
     */
    public void disablePlugin(String context , Exception error){
        logger.log(Level.SEVERE , context , error);
        this.getServer().getPluginManager().disablePlugin(this);
    }

    /**
     * Get current GameVillagerFactory
     * @return current GameVillagerFactory of the plugin
     * @see GameVillagerFactory
     */
    public GameVillagerFactory getFactory() {
        return factory;
    }

    /**
     * Get current BundleManager
     * @return current BundleManager of the plugin
     * @see BundleManager
     */
    public BundleManager getBundleManager() {
        return bundleManager;
    }

    /**
     * Get current ArenaManager
     * @return current ArenaManager
     * @see ArenaManager
     */
    public ArenaManager getArenaManager() {
        return arenaManager;
    }

    /**
     * Get current area manager
     * @return current area manager
     * @see PlayerArea
     */
    public PlayerArea getPlayerAreaManager() {
        return playerAreaManager;
    }

    /**
     * get current Locale Provider
     * @return current LocaleProvider
     * @see LocaleProvider
     */
    public LocaleProvider getLocaleProvider() {
        return localeProvider;
    }

    /**
     * Set current locale provider
     * @param localeProvider current locale provider
     */
    public void setLocaleProvider(LocaleProvider localeProvider) {
        this.localeProvider = localeProvider;
    }

    /**
     * Get current gson serialization instance
     * @return gson instance
     */
    public Gson getGson() {
        return gson;
    }

    /**
     * Get current thread pool of this plugin
     * @return current thread pool
     */
    public ScheduledExecutorService getThreadPool() {
        return threadPool;
    }

    /**
     * Get current GameManager
     * @return current GameManager
     */
    public GameManager getGameManager() {
        return gameManager;
    }

    /**
     * Get lobby wait time
     * @return lobby wait time
     */
    public int getWaitTime() {
        return waitTime;
    }

    /**
     * Get squared distance to check villager
     * @return squared distance to check villager
     */
    public double getSquaredDistanceCheck() {
        return squaredDistanceCheck;
    }

    /**
     * Get Red villager spawn chance
     * @return red spawn chance
     */
    public double getRedSpawnChance() {
        return redSpawnChance;
    }

    /**
     * Get point amount to win
     * @return point amount to win
     */
    public int getPointToWin() {
        return pointToWin;
    }
}
