package xyz.deltaevo.villagercolor.java;

/**
 * An object container
 * @param <T> value of this container
 */
public class Container<T> {
    private T value;

    /**
     * Create new container holding value
     * @param value , value to hold
     */
    public Container(T value) {
        this.value = value;
    }

    /**
     * Create a container holding null
     */
    public Container() {
        this(null);
    }

    /**
     * Get container value
     * @return container value
     */
    public T getValue() {
        return value;
    }

    /**
     * Set container value
     * @param value container value
     */
    public void setValue(T value) {
        this.value = value;
    }
}
