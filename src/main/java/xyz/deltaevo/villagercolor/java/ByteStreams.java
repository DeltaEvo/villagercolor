package xyz.deltaevo.villagercolor.java;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ByteStreams {

    public static final int BUF_SIZE = 8192;

    /**
     * DON'T USE THIS CONSTRUCTOR
     * @throws IllegalAccessException because there is no instance for you :p
     */
    private ByteStreams() throws IllegalAccessException {
        throw new IllegalAccessException("No instance for you");
    }

    /**
     * Copy a byte stream to an other
     * @param from stream to be copied
     * @param to stream to receive copy
     * @throws IOException if error occur
     */
    public static void copy(InputStream from , OutputStream to) throws IOException {
        int read;
        byte[] buff = new byte[BUF_SIZE];

        while ((read = from.read(buff, 0, buff.length)) != -1)
            to.write(buff, 0, read);
        to.flush();
    }

    /**
     * Transform this stream data to byte array
     * @param in the input stream to be transformed
     * @return byte array with data of the stream
     * @throws IOException if an error occur
     */
    public static byte[] toByteArray(InputStream in) throws IOException{
        ByteArrayOutputStream out = new ByteArrayOutputStream(Math.max(32, in.available()));
        copy(in , out);
        return out.toByteArray();
    }
}
