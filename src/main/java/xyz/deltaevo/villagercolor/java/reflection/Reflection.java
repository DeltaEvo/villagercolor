package xyz.deltaevo.villagercolor.java.reflection;

import xyz.deltaevo.villagercolor.VillagerColor;

import java.lang.reflect.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

public class Reflection {

    private static Map<Class<?> , Class<?>> swapper = new HashMap<>();

    static {
        addToSwapper(int.class, Integer.class);
        addToSwapper(long.class , Long.class);
        addToSwapper(double.class , Double.class);
        addToSwapper(float.class , Float.class);
        addToSwapper(boolean.class , Boolean.class);
        addToSwapper(char.class , Character.class);
        addToSwapper(byte.class , Byte.class);
        addToSwapper(short.class , Short.class);
    }

    /**
     * DON'T USE THIS CONSTRUCTOR
     * @throws IllegalAccessException because there is no instance for you :p
     */
    private Reflection() throws IllegalAccessException { throw new IllegalAccessException("No instance for you");}

    /**
     * Add class to swapper
     * @param clazz first class to swap
     * @param clazz2 second class to swap
     */
    private static void addToSwapper(Class<?> clazz , Class<?> clazz2){
        swapper.put(clazz , clazz2);
        swapper.put(clazz2 , clazz);
    }

    /**
     * Swap class (Primitive to Object's Primitive)
     * @param clazz class to swap
     * @return swapped class
     */
    public static Class<?> swapPrimitive(Class<?> clazz){
        return swapper.getOrDefault(clazz , clazz);
    }

    /**
     * Get constructor of class
     * @param clazz class to get constructor
     * @param parameterTypes constructor parameters
     * @param <T> class type
     * @return the constructor is found or null
     */
    @SuppressWarnings("unchecked")
    public static <T> Constructor<T> getConstructor(Class<T> clazz , Class<?> ... parameterTypes){
        return StreamCloser.of(Arrays.stream(clazz.getConstructors()) ,
                s -> (Constructor<T>) s.filter(c ->compare(c.getParameterTypes() , parameterTypes))
                .findFirst().orElse(null));
    }

    /**
     * Get constructor of class
     * @param type class to get constructor
     * @param parameterTypes constructor parameters
     * @param <T> class type
     * @return the constructor is found or null
     */
    public static <T> Constructor<T> getConstructor(ParamClass<T> type, Class<?> ... parameterTypes){
        return getConstructor(type.getClazz() , parameterTypes);
    }

    /**
     * Create new Class instance
     * @param clazz class to create new instance
     * @param arguments arguments to pass to constructor
     * @param <T> class type
     * @return instance created or null
     */
    public static <T> T newInstance(Class<T> clazz , Object... arguments){
        try {
            return getConstructor(clazz , getClass(arguments)).newInstance(arguments);
        } catch (ReflectiveOperationException e) {
            VillagerColor.logger.log(Level.INFO , "Could not create instance" , e);
            return null;
        }
    }

    /**
     * Create new Class instance
     * @param type class to create new instance
     * @param arguments arguments to pass to constructor
     * @param <T> class type
     * @return instance created or null
     */
    public static <T> T newInstance(ParamClass<T> type , Object... arguments){
        return newInstance(type.getClazz() , arguments);
    }

    /**
     * Get class's method
     * @param clazz class to get method
     * @param methodName name of the method
     * @param returnType return type of the function ( give Object.class if you want everything)
     * @param parameterTypes method parameters types
     * @return Method found or null
     */
    public static Method getMethod(Class<?> clazz , String methodName , Class<?> returnType,  Class<?> ... parameterTypes){
        if(clazz == null)
            return null;
        Method method = StreamCloser.of(Arrays.stream(clazz.getDeclaredMethods()) , s -> s.filter(m -> m.getName().equals(methodName) &&
                compare(m.getReturnType() , returnType) && compare(m.getParameterTypes()
                , parameterTypes)).findFirst().orElse(getMethod(clazz.getSuperclass() , methodName , returnType , parameterTypes)));
        if(method != null && !method.isAccessible())
            method.setAccessible(true);
        return method;
    }

    /**
     * Get class's method
     * @param clazz class to get method
     * @param methodName name of the method
     * @param parameterTypes method parameters types
     * @return Method found or null
     */
    public static Method getMethod(Class<?> clazz , String methodName , Class<?> ... parameterTypes){
        return getMethod(clazz , methodName , Object.class , parameterTypes);
    }

    /**
     * Invoke class's method
     * @param instance instance of the object to invoke method
     * @param methodName name of the method
     * @param returnType return type of the function ( give Object.class if you want everything)
     * @param arguments arguments to pass to the function
     * @param <T> return type
     * @return method return
     */
    public static <T> T invoke(Object instance , String methodName  , Class<T> returnType , Object ... arguments){
        try {
            return returnType.cast(getMethod(instance.getClass() , methodName , returnType ,getClass(arguments)).invoke(instance , arguments));
        } catch (ReflectiveOperationException e) {
            VillagerColor.logger.log(Level.INFO , "Could not invoke method" , e);
            return null;
        }
    }

    /**
     * Invoke class's method
     * @param instance instance of the object to invoke method
     * @param methodName name of the method
     * @param returnType return type of the function ( give Object.class if you want everything)
     * @param arguments arguments to pass to the function
     * @param <T> return type
     * @return method return
     */
    public static <T> T invoke(Object instance , String methodName  , ParamClass<T> returnType , Object ... arguments){
        return invoke(instance , methodName , returnType.getClazz() , arguments);
    }

    /**
     * Invoke class's method
     * @param instance instance of the object to invoke method
     * @param methodName name of the method
     * @param arguments arguments to pass to the function
     * @return method return
     */
    public static Object invoke(Object instance , String methodName  ,  Object ... arguments){
        return invoke(instance , methodName , Object.class , arguments);
    }

    /**
     * Get class field
     * @param clazz class to get field
     * @param fieldName name of the field
     * @param fieldType type of the field
     * @return Field instance if found or null
     */
    public static Field getField(Class<?> clazz , String fieldName ,  Class<?> fieldType){
        if(clazz == null)
            return null;
        Field field = StreamCloser.of(Arrays.stream(clazz.getDeclaredFields()) , s -> s.filter(f -> f.getName().equals(fieldName) &&
                compare(f.getType() , fieldType)).findFirst().orElse(getField(clazz.getSuperclass() , fieldName , fieldType)));
        if(field != null && !field.isAccessible())
            field.setAccessible(true);
        return field;
    }

    /**
     * Get static field value
     * @param fieldClass class of the field
     * @param fieldName name of the field
     * @param fieldType type of the field
     * @param <T> value type
     * @return field value
     */
    public static <T> T get(Class<?> fieldClass , String fieldName , Class<T> fieldType){
        try {
            return fieldType.cast(getField(fieldClass , fieldName , fieldType).get(null));
        } catch (ReflectiveOperationException e) {
            VillagerColor.logger.log(Level.INFO , "Could not get field " + fieldName + " on " + fieldClass , e);
            return null;
        }
    }

    /**
     * Get static field value
     * @param fieldClass class of the field
     * @param fieldName name of the field
     * @param fieldType type of the field
     * @param <T> value type
     * @return field value
     */
    public static <T> T get(Class<?> fieldClass , String fieldName , ParamClass<T> fieldType){
        return get(fieldClass , fieldName , fieldType.getClazz());
    }


    /**
     * Get object field value
     * @param instance instance of the object t
     * @param fieldName name of the field
     * @param fieldType type of the field
     * @param <T> value type
     * @return field value
     */
    public static <T> T get(Object instance , String fieldName , Class<T> fieldType){
        try {
            return fieldType.cast(getField(instance.getClass() , fieldName , fieldType).get(instance));
        } catch (ReflectiveOperationException e) {
            VillagerColor.logger.log(Level.INFO , "Could not get field " + fieldName + " on " + instance.getClass() , e);
            return null;
        }
    }

    /**
     * Get object field value
     * @param instance instance of the object t
     * @param fieldName name of the field
     * @param fieldType type of the field
     * @param <T> value type
     * @return field value
     */
    public static <T> T get(Object instance , String fieldName , ParamClass<T> fieldType){
        return get(instance , fieldName , fieldType.getClazz());
    }

    /**
     * Get object field value
     * @param instance instance of the object t
     * @param fieldName name of the field
     * @return field value
     */
    public static Object get(Object instance , String fieldName){
        return get(instance , fieldName , Object.class);
    }

    /**
     * Set static field value
     * @param fieldClass class of the field
     * @param fieldName name of the field
     */
    public static void set(Class<?>  fieldClass, String fieldName , Object value){
        try {
            getField(fieldClass , fieldName , value.getClass()).set(null , value);
        } catch (IllegalAccessException | NullPointerException e) {
            VillagerColor.logger.log(Level.INFO , "Could not set field " + fieldName + " on " + fieldClass , e);

        }
    }
    /**
     *
     * Set object's field value
     * @param instance instance of the object to set value
     * @param fieldName name of the field
     */
    public static void set(Object instance , String fieldName , Object value){
        try {
            getField(instance.getClass() , fieldName , value.getClass()).set(instance , value);
        } catch (IllegalAccessException | NullPointerException e) {
            VillagerColor.logger.log(Level.INFO , "Could not set field " + fieldName + " on " + instance.getClass() , e);
        }
    }

    /**
     * Get classes of this objects
     * @param objs objects to get classes
     * @return objects classes
     */
    public static Class<?>[] getClass(Object... objs){
        return (Class<?>[]) StreamCloser.of(Arrays.stream(objs) , s -> s.map(o -> o == null ? Object.class : o.getClass()).toArray(Class[]::new));
    }

    /**
     * Compare two classes , check if is the same or a super class , or a primitive object
     * @param clazz first class to compare
     * @param clazz2 second class to compare
     * @return if it's "the same"
     */
    public static boolean compare(Class<?> clazz , Class<?> clazz2){
            return clazz2.equals(Object.class) ||
                    clazz.isAssignableFrom(clazz2) ||
                    clazz2.isAssignableFrom(clazz) ||
                    compareWithoutSwap(swapPrimitive(clazz) , swapPrimitive(clazz2));
    }

    /**
     * Compare two classes , check if is the same or a super class
     * @param clazz first class to compare
     * @param clazz2 second class to compare
     * @return if it's "the same"
     */
    public static boolean compareWithoutSwap(Class<?> clazz , Class<?> clazz2){
        return clazz2.equals(Object.class) ||
                clazz.isAssignableFrom(clazz2) ||
                clazz2.isAssignableFrom(clazz);
    }

    /**
     * Compare class array using Reflection.compare()
     * @param clazz first class array
     * @param clazz2 second class array
     * @return if all match to Reflection.compare()
     * @see Reflection#compare(Class, Class)
     */
    public static boolean compare(Class<?>[] clazz , Class<?>[] clazz2){
        if(clazz.length != clazz2.length)
            return false;
        for(int i = 0 ; i < clazz.length ; i++)
            if(!compare(clazz[i] , clazz2[i]))
                return false;
        return true;
     }

    /**
     * Compare types array using Reflection.compare()
     * @param types first type array
     * @param types2 second type array
     * @return if all match to Reflection.compare()
     * @see Reflection#compare(Class, Class)
     * @see Reflection#getClazz(Type)
     */
    public static boolean compare(Type[] types , Type[] types2){
        if(types.length != types2.length)
            return false;
        for(int i = 0 ; i < types.length ; i++)
            if(!compare(getClazz(types[i]) , getClazz(types2[i])))
                return false;
        return true;
    }

    /**
     * Get Type Class
     * @param type type to get Class
     * @param <T> return type
     * @return Class
     */
    @SuppressWarnings("unchecked")
    public static<T> Class<T> getClazz(Type type){
        if(type instanceof Class<?>)
            return (Class<T>)type;
        else if(type instanceof ParameterizedType)
            return (Class<T>)((ParameterizedType)type).getRawType();
        else
            return null;
    }
}
