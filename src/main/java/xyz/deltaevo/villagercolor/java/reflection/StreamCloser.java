package xyz.deltaevo.villagercolor.java.reflection;

import java.util.stream.Stream;

@FunctionalInterface
public interface StreamCloser<T , U> {
    /**
     * Use this stream
     * @param stream stream to be used
     * @return what you want
     */
    T use(Stream<U> stream);

    /**
     * Create new Stream Closer with specified elements
     * @param stream stream to be used
     * @param closer closer to consume the stream
     * @param <T> return type of the closer
     * @param <U> type of stream data
     * @return return of closer
     */
    static <T , U> T of(Stream<U> stream , StreamCloser<T , U> closer){
        T value = closer.use(stream);
        stream.close();
        return value;
    }
}
