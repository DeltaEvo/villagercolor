package xyz.deltaevo.villagercolor.java.reflection;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public abstract class ParamClass<T> {
    private final Type type;

    /**
     * Create new paramClass using TypeArguments
     */
    protected ParamClass() {
        if (getClass().getGenericSuperclass() instanceof ParameterizedType) {
            this.type = ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        }
        else this.type = null;
    }

    /**
     * Create new instance of this ParamClass
     * @param args args to be passed to the constructor
     * @return instance of this class
     * @see Reflection#newInstance(ParamClass, Object...)
     */
    public final T newInstance(Object...args){
        return Reflection.newInstance(getClazz() , args);
    }

    /**
     * Return type of this ParamClass
     * @return type
     */
    public final Type getType() {
        return this.type;
    }

    /**
     * Return class of the type of this ParamClass
     * @return class
     */
    public final Class<T> getClazz(){
        return Reflection.<T>getClazz(type);
    }
}
