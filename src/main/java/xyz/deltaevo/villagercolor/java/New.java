package xyz.deltaevo.villagercolor.java;

import xyz.deltaevo.villagercolor.java.reflection.ParamClass;
import xyz.deltaevo.villagercolor.java.reflection.Reflection;

import java.util.*;

public class New {

    /**
     * DON'T USE THIS CONSTRUCTOR
     * @throws IllegalAccessException because there is no instance for you :p
     */
    private New() throws IllegalAccessException {
        throw new IllegalAccessException("No instance for you");
    }

    /**
     * Create new Instance using reflection (equal to Reflection.newInstance)
     * @param clazz class of the object
     * @param args args to give to object constructor
     * @param <T> type of object to be created
     * @return instance created using reflection
     * @see Reflection#newInstance(Class, Object...)
     */
    public static <T> T usingReflection(Class<T> clazz , Object...args){
        return Reflection.newInstance(clazz , args);
    }

    /**
     * Create new Instance using reflection (equal to Reflection.newInstance)
     * @param clazz class of the object
     * @param args args to give to object constructor
     * @param <T> type of object to be created
     * @return instance created using reflection
     * @see Reflection#newInstance(ParamClass, Object...)
     */
    public static <T> T usingReflection(ParamClass<T> clazz , Object...args){
        return Reflection.newInstance(clazz , args);
    }

    /**
     * Create new collection with specified elements
     * @param clazz class of the collection
     * @param t data to add to the collection
     * @param <T> type of data
     * @param <U> type of collection
     * @return collection with specified elements
     */
    public static<T , U extends Collection<T>> U collection(ParamClass<U> clazz , T...t){
        U coll = clazz.newInstance();
        for(T o : t)
            coll.add(o);
        return coll;
    }

    /**
     * Create new List with specified Elements
     * @param t data to add to the list
     * @param <T> type of the data
     * @return a list with this datas
     */
    public static<T> List<T> list(T...t){
        return collection(new ParamClass<ArrayList<T>>(){}, t);
    }

    /**
     * Create new Set with specified Elements
     * @param t data to add to the Set
     * @param <T> type of the data
     * @return a set with this datas
     */
    public static<T> Set<T> set(T...t){
        return collection(new ParamClass<HashSet<T>>(){}, t);
    }

    /**
     * Create new Array with specified Elements
     * @param t data to add to the array
     * @param <T> type of the data
     * @return a array with this datas
     */
    public static<T> T[] array(T...t){
        return t;
    }

    /**
     * Transform collection to Array
     * @param t collection
     * @param <T> collection data type
     * @return array containing element of this collection
     */
    @SuppressWarnings("unchecked")
    public static<T> T[] array(Collection<T> t){
        return t.toArray((T[])new Object[t.size()]);
    }

}
