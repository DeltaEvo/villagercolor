package xyz.deltaevo.villagercolor.arena;

import org.bukkit.Location;

public class Path {
    private Location playerLocation;
    private Location from;
    private Location to;

    /**
     * Create new villager Path
     * @param playerLocation player spawn location
     * @param from from path
     * @param to to path
     */
    public Path(Location playerLocation, Location from, Location to) {
        this.playerLocation = playerLocation;
        this.from = from;
        this.to = to;
    }

    /**
     * Get player spawn location
     * @return player spawn location
     */
    public Location getPlayerLocation() {
        return playerLocation;
    }

    /**
     * Get path from location
     * @return player path from location
     */
    public Location getFrom() {
        return from;
    }

    /**
     * Get path to location
     * @return player path to location
     */
    public Location getTo() {
        return to;
    }
}
