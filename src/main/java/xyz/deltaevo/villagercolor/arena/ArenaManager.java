package xyz.deltaevo.villagercolor.arena;

import com.google.gson.Gson;
import xyz.deltaevo.villagercolor.VillagerColor;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.logging.Level;
import java.util.stream.Stream;

/**
 * Manage Arena Instance:
 *  -Load them
 *  -Save them
 */
public class ArenaManager {
    private final File folder;
    private Gson gson;
    private Collection<Arena> arenas;
    private Collection<ArenaListener> listeners;

    /**
     * Create new ArenaManager using the specified folder
     * @param folder folder to save and load Arena
     * @param gson instance of Gson used to serialize
     */
    public ArenaManager(File folder , Gson gson) {
        this.folder = folder;
        this.gson = gson;
        this.arenas = new HashSet<>();
        this.listeners = new HashSet<>();
        this.folder.mkdirs();
    }

    /**
     * Get Arena By Name
     * @param name name of the Arena
     * @param ignoreCase check name ignoring case
     * @return Arena if found or null
     */
    public Arena getByName(String name , boolean ignoreCase){
        if(name == null)
            return null;
        for(Arena arena : arenas){
            if(ignoreCase ? name.equalsIgnoreCase(arena.getName()) : name.equals(arena.getName()))
                return arena;
        }
        return null;
    }

    /**
     * Add Arena to this ArenaManager
     * @param arena arena to add
     */
    public void addArena(Arena arena){
        arenas.add(arena);
        listeners.forEach(l -> l.onAdd(arena));
    }

    /**
     * Remove Arena from this manager
     * @param arena arena to remove
     */
    public void removeArena(Arena arena){
        arenas.remove(arena);
        listeners.forEach(l -> l.onRemove(arena));
    }

    /**
     * Delete Arena File
     * @param arena arena to be deleted
     */
    public void deleteArena(Arena arena){
        getFile(arena).delete();
    }

    /**
     * Load Arenas of this manager folder
     */
    public void loadArenas(){
        if(folder.listFiles() == null)
            return;
        Stream<File> stream = Arrays.stream(folder.listFiles());
        stream.map(this::loadArena).forEach(this::addArena);
        stream.close();
    }

    /**
     * Load arena of this file
     * @param file file to be loaded
     * @return Arena
     */
    public Arena loadArena(File file){
        try {
            return gson.fromJson(new String(Files.readAllBytes(Paths.get(file.toURI()))) , Arena.class);
        } catch (IOException e) {
            VillagerColor.logger.log(Level.INFO , "Can't read file " + file , e);
            return null;
        }
    }

    /**
     * Save and unload all arenas of the manager
     */
    public void unloadArenas(){
        saveArenas();
        arenas.clear();
    }

    /**
     * Save all Arena of the manager
     */
    public void saveArenas(){
        arenas.stream().forEach(this::saveArena);
    }

    /**
     * Save arena
     * @param arena arena to be saved
     */
    public void saveArena(Arena arena){
        saveArena(arena , getFile(arena));
    }

    /**
     * Save arena to this file
     * @param arena arena to be saved
     * @param file file to save arena
     */
    public void saveArena(Arena arena , File file){
        try {
            Files.write(Paths.get(file.toURI()) , gson.toJson(arena).getBytes());
        } catch (IOException e) {
            VillagerColor.logger.log(Level.INFO , "Error when writing to file " + file , e);
        }
    }

    /**
     * Get current file of this arena
     * @param arena arena to get File
     * @return file of this arena
     */
    public File getFile(Arena arena){
        return new File(folder , arena.getName() + ".json");
    }


    /**
     * Get all arena of this ArenaManager
     * @return unmodifiable Collection of Arena
     */
    public Collection<Arena> getArenas() {
        return Collections.unmodifiableCollection(arenas);
    }

    public void addListener(ArenaListener listener){
        listeners.add(listener);
    }

    public void removeListener(ArenaListener listener){
        listeners.remove(listener);
    }

    public interface ArenaListener{
        void onAdd(Arena arena);
        void onRemove(Arena arena);
    }
}
