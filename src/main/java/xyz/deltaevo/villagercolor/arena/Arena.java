package xyz.deltaevo.villagercolor.arena;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import xyz.deltaevo.villagercolor.area.Area;
import xyz.deltaevo.villagercolor.i18n.Variables;

import java.util.*;
import java.util.stream.Collectors;

public class Arena{
    private String name;
    private Area area;
    private List<Path> paths;
    private Location lobby;
    private int minPlayers;


    /**
     * Create a new Arena with specified parameters
     * @param name name
     * @param area area
     * @param paths villagers paths
     */
    public Arena(String name, Area area, List<Path> paths) {
        this.name = name;
        this.area = area;
        this.paths = paths;
    }
    /**
     * Create a new Arena with specified parameters
     * @param name name
     * @param area are
     */
    public Arena(String name, Area area) {
        this(name , area , new ArrayList<>());
    }

    /**
     * Get the name
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Get Area
     * @return area
     */
    public Area getArea() {
        return area;
    }


    /**
     * Get villager paths
     * @return paths
     */
    public List<Path> getPaths() {
        return paths;
    }

    /**
     * Set lobby location
     * @param lobby lobby location
     */
    public void setLobby(Location lobby) {
        this.lobby = lobby;
    }

    /**
     * Get lobby location
     * @return lobby location
     */
    public Location getLobby() {
        return lobby;
    }

    /**
     * Set minimum player amount
     * @param minPlayers minimum player amount
     */
    public void setMinPlayers(int minPlayers) {
        this.minPlayers = minPlayers;
    }

    /**
     * Get minimum player amount
     * @return minimum player amount
     */
    public int getMinPlayers() {
        return minPlayers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Arena arena = (Arena) o;
        return Objects.equals(name, arena.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    /**
     * Get Description of this Area
     * @param bundle RessourceBundle to build the description
     * @return the description
     */
    public String getDescription(ResourceBundle bundle) {
        String locationFormat = bundle.getString(Variables.PATH_LOCATION_FORMAT);
        return ChatColor.translateAlternateColorCodes('&',
                bundle.getString("arenaDescription")
                        .replace("{name}" , getName())
                        .replace("{lobby}" , Variables.replaceLocation(locationFormat , lobby))
                        .replace("{minPlayers}" , Integer.toString(minPlayers))
                        .replace("{paths}" , paths.stream()
                            .map(p -> Variables.replaceLocation(locationFormat , p.getPlayerLocation()) + " : "+
                                    Variables.replaceLocation(locationFormat , p.getFrom())
                                + " -> "+Variables.replaceLocation(locationFormat , p.getTo()))
                        .collect(Collectors.joining(", "))));
    }
}
