package xyz.deltaevo.villagercolor;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import xyz.deltaevo.villagercolor.area.AreaCreator;
import xyz.deltaevo.villagercolor.arena.Arena;
import xyz.deltaevo.villagercolor.arena.Path;
import xyz.deltaevo.villagercolor.command.Command;
import xyz.deltaevo.villagercolor.command.CommandHandler;
import xyz.deltaevo.villagercolor.command.CommandListener;
import xyz.deltaevo.villagercolor.i18n.Variables;
import xyz.deltaevo.villagercolor.selector.Selector;

import java.util.ResourceBundle;

public class VillagerColorCommands implements CommandListener{

    private final VillagerColor instance;

    /**
     * Create new VillagerColorCommands instance for specified VillagerColor
     * @param instance VillagerColor instance
     */
    public VillagerColorCommands(VillagerColor instance) {
        this.instance = instance;
    }

    @CommandHandler(args = {"spawn" , CommandHandler.UNDEFINED , CommandHandler.UNDEFINED , CommandHandler.UNDEFINED})
    public boolean spawn(Command cmd){
        if(!(cmd.getSender() instanceof Player))
            return false;

        Player player = (Player)cmd.getSender();

        instance.getFactory().create(player.getLocation()).goTo(new Location(player.getWorld() ,
                Integer.valueOf(cmd.getArgs()[1]) , Integer.valueOf(cmd.getArgs()[2]) , Integer.valueOf(cmd.getArgs()[3])),
                0.5);
        return true;
    }

    @CommandHandler(args = {"arena" , "create" , CommandHandler.UNDEFINED})
    public boolean createArena(Command cmd){
        if(!(cmd.getSender() instanceof Player))
            return false;

        Player player = (Player)cmd.getSender();

        AreaCreator creator = instance.getPlayerAreaManager().getCreator(player);

        ResourceBundle bundle =
                instance.getBundleManager().getBundle(instance.getLocaleProvider().getLocale(player));

        if(creator == null){
            player.sendMessage(instance.getPlayerAreaManager().getHelp(bundle));
            return false;
        }

        if(!creator.isValid()){
            player.sendMessage(creator.nonValidMessage(bundle));
            return false;
        }

        if(instance.getArenaManager().getByName(cmd.getArgs()[2] , true) != null){
            player.sendMessage(bundle.getString("arenaAlreadyExist").replace(Variables.NAME , cmd.getArgs()[2]));
            return false;
        }
        Arena arena = new Arena(cmd.getArgs()[2] , creator.getArea());

        instance.getArenaManager().addArena(arena);
        instance.getArenaManager().saveArena(arena);

        player.sendMessage(arena.getDescription(bundle));

        return true;
    }

    @CommandHandler(args = {"arena" , "save" , CommandHandler.UNDEFINED})
    public boolean saveArena(Command cmd){
        if(!(cmd.getSender() instanceof Player))
            return false;

        Player player = (Player)cmd.getSender();

        ResourceBundle bundle =
                instance.getBundleManager().getBundle(instance.getLocaleProvider().getLocale(player));

        Arena arena = getArena(cmd.getArgs()[1] , player , bundle);
        if(arena == null)
            return false;

        instance.getArenaManager().saveArena(arena);

        player.sendMessage(bundle.getString("arenaSaved").replace(Variables.NAME , arena.getName()));

        return true;
    }

    @CommandHandler(args = {"arena"  , CommandHandler.UNDEFINED ,"setLobby"})
    public boolean setAreaLobby(Command cmd){
        if(!(cmd.getSender() instanceof Player))
            return false;

        Player player = (Player)cmd.getSender();

        ResourceBundle bundle =
                instance.getBundleManager().getBundle(instance.getLocaleProvider().getLocale(player));

        Arena arena = getArena(cmd.getArgs()[1] , player , bundle);
        if(arena == null)
            return false;

        arena.setLobby(player.getLocation());

        player.sendMessage(bundle.getString("lobbySet")
                .replace(Variables.NAME , arena.getName()).replace("{loc}" ,
                        Variables.replaceLocation(bundle.getString(Variables.PATH_LOCATION_FORMAT) , player.getLocation())));


        return true;
    }

    @CommandHandler(args = {"arena"  , CommandHandler.UNDEFINED ,"minPlayers" , CommandHandler.UNDEFINED})
    public boolean setAreaMinPlayers(Command cmd){
        if(!(cmd.getSender() instanceof Player))
            return false;

        Player player = (Player)cmd.getSender();

        ResourceBundle bundle =
                instance.getBundleManager().getBundle(instance.getLocaleProvider().getLocale(player));

        Arena arena = getArena(cmd.getArgs()[1] , player , bundle);
        if(arena == null)
            return false;

        try{
            int minPlayers = Integer.parseInt(cmd.getArgs()[3]);
            arena.setMinPlayers(minPlayers);
            player.sendMessage(bundle.getString("minPlayersSet")
                    .replace(Variables.NAME , arena.getName()).replace("{minPlayers}" ,cmd.getArgs()[3]));


        }catch (NumberFormatException exception){
            player.sendMessage(bundle.getString("unknownNumber").replace("{number}" , cmd.getArgs()[3]));
        }

        return true;
    }

    @CommandHandler(args = {"arena"  , CommandHandler.UNDEFINED ,"addPath"})
    public boolean addPathToArea(Command cmd){
        if(!(cmd.getSender() instanceof Player))
            return false;

        Player player = (Player)cmd.getSender();

        ResourceBundle bundle =
                instance.getBundleManager().getBundle(instance.getLocaleProvider().getLocale(player));

        Arena arena = getArena(cmd.getArgs()[1] , player , bundle);
        if(arena == null)
            return false;

        Selector selector = Selector.get(player , false);

        if(!selector.hasTwoLocations()){
            player.sendMessage(selector.getHelp(bundle));
            return false;
        }

        //Add check for door or barrier

        arena.getPaths().add(new Path(player.getLocation() , selector.getFirstLocation() , selector.getSecondLocation()));

        String locationFormat = bundle.getString(Variables.PATH_LOCATION_FORMAT);
        player.sendMessage(bundle.getString("pathAdded")
                .replace(Variables.NAME , arena.getName()).replace("{path}" ,
                        Variables.replaceLocation(locationFormat, player.getLocation()) + " : " +
                        Variables.replaceLocation(locationFormat, selector.getFirstLocation())
        + " -> " + Variables.replaceLocation(locationFormat, selector.getSecondLocation())));


        return true;
    }

    @CommandHandler(args = {"arena"  , CommandHandler.UNDEFINED ,"info" })
    public boolean areaInfo(Command cmd){
        if(!(cmd.getSender() instanceof Player))
            return false;

        Player player = (Player)cmd.getSender();
        ResourceBundle bundle =
                instance.getBundleManager().getBundle(instance.getLocaleProvider().getLocale(player));

        Arena arena = getArena(cmd.getArgs()[1] , player , bundle);
        if(arena == null)
            return false;

        player.sendMessage(arena.getDescription(bundle));

        return true;
    }

    @CommandHandler(args = {"selector"})
    public boolean selector(Command cmd){
        if(!(cmd.getSender() instanceof Player))
            return false;

        Player player = (Player)cmd.getSender();

        Selector.get(player , true);

        return true;
    }

    @CommandHandler(args = {"games"})
    public boolean games(Command cmd){
        if(!(cmd.getSender() instanceof Player))
            return false;

        Player player = (Player)cmd.getSender();

        instance.getGameManager().openWindowFor(player);

        return true;
    }

    private Arena getArena(String name , Player player , ResourceBundle bundle){
        Arena arena = instance.getArenaManager().getByName(name , true);
        if(arena == null){
            player.sendMessage(bundle.getString(Variables.PATH_UNKNOWN_ARENA)
                    .replace(Variables.NAME , name));
        }
        return arena;

    }


}
