package xyz.deltaevo.villagercolor.window;

import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.inventory.InventoryEvent;
import org.bukkit.inventory.InventoryView;

public class InventoryCancelableCloseEvent extends InventoryEvent implements Cancellable {
    private static final HandlerList handlers = new HandlerList();
    private boolean cancelled = false;

    /**
     * Create new InventoryCancelableCloseEvent
     * @param transaction view of this event
     */
    public InventoryCancelableCloseEvent(InventoryView transaction) {
        super(transaction);
    }

    @Override
    public boolean isCancelled() {
        return this.cancelled;
    }

    @Override
    public void setCancelled(boolean cancel) {
        this.cancelled = cancel;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    /**
     * Get handler list of this Event used by bukkit
     * @return HandlerList
     */
    public static HandlerList getHandlerList() {
        return handlers;
    }
}
