package xyz.deltaevo.villagercolor.window;

import java.util.Arrays;
import java.util.HashMap;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Window {
    private Inventory inventory;
    private boolean closeable;
    private HashMap<Integer, WindowAction> actions = new HashMap<>();
    private WindowView view;

    public Window(String name, InventoryType type, boolean closeable) {
        this.inventory = Bukkit.createInventory(null, type, name);
        this.closeable = closeable;
    }

    public Window(Inventory inv, boolean closeable) {
        this.inventory = inv;
        this.closeable = closeable;
    }

    public Window(String name, int size, boolean closeable) {
        this.inventory = Bukkit.createInventory(null, size, name);
        this.closeable = closeable;
    }

    public String getName() {
        return this.inventory.getName();
    }

    public InventoryType getType() {
        return this.inventory.getType();
    }

    public ItemStack[] getContents() {
        return this.inventory.getContents();
    }

    public Inventory getInventory() {
        return this.inventory;
    }

    public void addItem(int pos, ItemStack is, WindowAction action) {
        this.inventory.setItem(pos, is);
        this.actions.put(pos, action);
    }

    public boolean isCloseable() {
        return this.closeable;
    }

    public void setCloseable(boolean closeable) {
        this.closeable = closeable;
    }

    public WindowView getView() {
        return this.view;
    }

    void setView(WindowView view) {
        this.view = view;
    }

    protected ItemStack newIS(Material type, String name, String... lores) {
        ItemStack is = new ItemStack(type);
        ItemMeta im = is.getItemMeta();
        im.setDisplayName(name);
        im.setLore(Arrays.asList(lores));
        is.setItemMeta(im);
        return is;
    }

    protected ItemStack newIS(Material type, byte datas, String name, String... lores) {
        ItemStack is = new ItemStack(type, 1, (short)datas);
        ItemMeta im = is.getItemMeta();
        im.setDisplayName(name);
        im.setLore(Arrays.asList(lores));
        is.setItemMeta(im);
        return is;
    }

    protected ItemStack newIS(Material type, int amount, String name, String... lores) {
        ItemStack is = new ItemStack(type, amount);
        ItemMeta im = is.getItemMeta();
        im.setDisplayName(name);
        im.setLore(Arrays.asList(lores));
        is.setItemMeta(im);
        return is;
    }

    protected ItemStack newIS(Material type, int amount, byte datas, String name, String... lores) {
        ItemStack is = new ItemStack(type, amount, (short)datas);
        ItemMeta im = is.getItemMeta();
        im.setDisplayName(name);
        im.setLore(Arrays.asList(lores));
        is.setItemMeta(im);
        return is;
    }

    public void onInventoryClick(InventoryClickEvent event) {
        event.setCancelled(true);
        if(this.actions.get(event.getSlot()) != null) {
            this.actions.get(event.getSlot()).run(event);
        }

    }

    public void onInventoryClose(InventoryCancelableCloseEvent event) {event.setCancelled(!closeable);}

    public void onInventoryDrag(InventoryDragEvent event) {
        event.setCancelled(true);
    }

    public void close(Player player) {
        player.getOpenInventory().close();
    }

    public void open(Player player) {
        player.openInventory(inventory);
    }

}