package xyz.deltaevo.villagercolor.window;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class WindowListener implements Listener {
    private JavaPlugin plugin;

    /**
     * Create new window listener
     * @param plugin JavaPlugin used for runnables
     */
    public WindowListener(JavaPlugin plugin) {
        this.plugin = plugin;
    }

    /**
     * On Inventory Click
     * @param event bukkit event
     */
    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        WindowView.instances.stream().filter(view -> view.getWindow().getInventory().equals(event.getClickedInventory())).forEach(view ->
            view.getWindow().onInventoryClick(event)
        );
    }

    /**
     * On Inventory Drag
     * @param event bukkit event
     */
    @EventHandler
    public void onInventoryDrag(InventoryDragEvent event) {
        WindowView.instances.stream().filter(view -> view.getWindow().getInventory().equals(event.getInventory())).forEach(view ->
            view.getWindow().onInventoryDrag(event)
        );

    }

    /**
     * On Inventory Close
     * @param event bukkit event
     */
    @EventHandler
    public void onInventoryClose(final InventoryCloseEvent event) {
        InventoryCancelableCloseEvent icce = new InventoryCancelableCloseEvent(event.getView());
        Bukkit.getPluginManager().callEvent(icce);
        if(icce.isCancelled()) {
            Bukkit.getScheduler().runTaskLater(this.plugin , () ->
                    event.getPlayer().openInventory(event.getInventory()), 1L);
        }

    }

    /**
     * On Inventory Cancel Close
     * @param event InventoryCancelableCloseEvent event
     */
    @EventHandler(priority = EventPriority.MONITOR)
    public void onInventoryCancelClose(InventoryCancelableCloseEvent event) {
        WindowView.instances.stream().filter(view -> view.getWindow() != null && view.getWindow().getInventory().equals(event.getInventory())).forEach(view -> {
            view.getWindow().onInventoryClose(event);
            if (!event.isCancelled()) WindowView.instances.remove(view);
        });

    }
}
