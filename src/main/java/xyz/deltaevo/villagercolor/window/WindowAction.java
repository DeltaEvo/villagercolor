package xyz.deltaevo.villagercolor.window;

import org.bukkit.event.inventory.InventoryClickEvent;

@FunctionalInterface
public interface WindowAction {
    /**
     * Run action on click
     * @param event event hapened on click
     */
    void run(InventoryClickEvent event);
}

