package xyz.deltaevo.villagercolor.window;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.bukkit.entity.Player;

public class WindowView {
    static List<WindowView> instances = new CopyOnWriteArrayList<>();
    private Window window;
    private Player player;

    /**
     * Create new WindowView for this player using specofed window
     * @param window window to set
     * @param player player holding the View
     */
    public WindowView(Window window, Player player) {
        instances.add(this);
        this.window = window;
        window.setView(this);
        this.player = player;
    }

    /**
     * Open window to player
     */
    public void open(){
        this.window.open(player);
    }

    /**
     * Set current player window
     * @param window window to set
     */
    public void setWindow(Window window) {
        this.window.setView(null);
        this.window = window;
        this.window.setView(this);
        this.window.close(player);
        open();
    }

    /**
     * Get current window of this view
     * @return
     */
    public Window getWindow() {
        return this.window;
    }
}
