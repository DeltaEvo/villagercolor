package xyz.deltaevo.villagercolor.command;

import java.lang.reflect.Method;
import java.util.List;
import java.util.logging.Level;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import xyz.deltaevo.villagercolor.VillagerColor;
import xyz.deltaevo.villagercolor.java.New;

public class CommandRegisterer implements CommandExecutor {
    private String help;
    private List<CommandListener> listeners;

    private CommandRegisterer(List<CommandListener> listeners, String help) {
        this.help = help;
        this.listeners = listeners;
    }

    /**
     * Register new command
     * @param command Bukkit Plugin Command
     * @param listener CommandLister implementation
     * @param help help phrase
     */
    public static CommandRegisterer register(PluginCommand command, CommandListener listener, String help) {
        CommandRegisterer registerer = new CommandRegisterer(New.list(listener), help);
        command.setExecutor(registerer);
        return registerer;
    }

    public List<CommandListener> getListeners() {
        return listeners;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        for(CommandListener listener : listeners)
            for(Method m : listener.getClass().getDeclaredMethods()) {
                if(!m.isAnnotationPresent(CommandHandler.class))
                    continue;
                CommandHandler anot = m.getAnnotation(CommandHandler.class);
                if(anot.infinite() ? args.length >= anot.args().length : args.length == anot.args().length
                        && checkArgs(anot.args()  , args))
                   return invokeMethod(listener , m , new xyz.deltaevo.villagercolor.command.Command(sender, cmd, label, args));

            }

        if(this.help != null && this.help.length() != 0) {
            sender.sendMessage(this.help);
        }

        return false;
    }

    private boolean checkArgs(String[] anotArgs , String[] args){
        for(int i = 0; i < anotArgs.length; i++)
            if(anotArgs[i].equals(CommandHandler.UNDEFINED))
                continue;
            else if(!anotArgs[i].equalsIgnoreCase(args[i]))
                return false;
        return true;
    }

    /**
     * Invoke method with command as parameter
     * @param method method to invoke
     * @param command command to give as parameter
     * @return if the command fail
     */
    private boolean invokeMethod(CommandListener listener ,Method method, xyz.deltaevo.villagercolor.command.Command command){
        try {
            Object o = method.invoke(listener, command);
            return !(o instanceof Boolean) || (boolean) o;
        } catch (Exception e) {
            VillagerColor.logger.log(Level.INFO , "Command Error" , e);
            return true;
        }
    }
}

