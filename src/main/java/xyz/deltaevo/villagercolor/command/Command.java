package xyz.deltaevo.villagercolor.command;

import org.bukkit.command.CommandSender;

public class Command {
    private CommandSender sender;
    private org.bukkit.command.Command cmd;
    private String label;
    private String[] args;

    /**
     * Create new command
     * @param sender sender of this command
     * @param cmd bukkit command
     * @param label command name
     * @param args command args
     */
    public Command(CommandSender sender, org.bukkit.command.Command cmd, String label, String[] args) {
        this.sender = sender;
        this.cmd = cmd;
        this.label = label;
        this.args = args;
    }

    /**
     * Get Command Sender
     * @return the command sender
     */
    public CommandSender getSender() {
        return this.sender;
    }

    /**
     * Get bukkit command
     * @return the bukkit command
     */
    public org.bukkit.command.Command getCmd() {
        return this.cmd;
    }

    /**
     * Get Command name
     * @return command name
     */
    public String getLabel() {
        return this.label;
    }

    /**
     * Get command args
     * @return command args
     */
    public String[] getArgs() {
        return this.args;
    }
}
