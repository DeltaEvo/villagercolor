package xyz.deltaevo.villagercolor.command;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface CommandHandler {
    String UNDEFINED = "\nnull\n";

    /**
     * Args to execute the command
     * @return args to execute the command
     */
    String[] args();

    /**
     * Infinite Command
     * @return if is an infinite command
     */
    boolean infinite() default false;
}

