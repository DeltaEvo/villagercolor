package xyz.deltaevo.villagercolor.selector;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import xyz.deltaevo.villagercolor.VillagerColor;
import xyz.deltaevo.villagercolor.i18n.Variables;

import java.util.*;

public class Selector {
    public static final ItemStack ITEM = new ItemStack(Material.GOLD_SWORD);
    private static Map<Player, Selector> instances = new HashMap<>();
    private static ArrayList<SelectorAdd> listeners = new ArrayList<>();

    private Location first;
    private Location second;

    /**
     * Create new selector for this player
     * @param player player to create Selector
     */
    public Selector(Player player) {
        if(player.getInventory().contains(ITEM)){
            player.getInventory().addItem(ITEM);
            player.updateInventory();
        }
    }

    /**
     * Set first location of this selector
     * @param first location to be set
     */
    public void setFirstLocation(Location first) {
        this.first = first;
    }

    /**
     * Get first location of this selector
     * @return location
     */
    public Location getFirstLocation(){
        return first;
    }

    /**
     * Set second location of this selector
     * @param second location to be set
     */
    public void setSecondLocation(Location second) {
        this.second = second;
    }

    /**
     * Get second location of this selector
     * @return location
     */
    public Location getSecondLocation(){
        return second;
    }

    /**
     * Check if has two locations
     * @return true of has two locations
     */
    public boolean hasTwoLocations(){
        return first != null && second != null;
    }

    /**
     * Get help to complete location
     * @param lang ResourceBundle to get message
     * @return help
     */
    public String getHelp(ResourceBundle lang) {
        if(getFirstLocation() == null)
            return lang.getString("selectorHelpLoc1");
        else if(getSecondLocation() == null)
            return lang.getString("selectorHelpLoc2");
        else return null;
    }

    /**
     * Listener for the selectors
     */
    public static class SelectorListener implements Listener{
        private VillagerColor instance;

        /**
         * Create new Selector Listener with VillagerColor instace
         * @param instance Plugin instance
         */
        public SelectorListener(VillagerColor instance) {
            this.instance = instance;
        }

        /**
         * On player interact event
         * @param event player interact event
         */
        @EventHandler
        public void onPlayerInteract(PlayerInteractEvent event){
            Player player = event.getPlayer();

            if(!instances.containsKey(player))
                return;

            Selector selector = get(player , false);

            ResourceBundle bundle =
                    instance.getBundleManager().getBundle(instance.getLocaleProvider().getLocale(player));

            ItemStack inHand = event.getItem();
            //CheckItem
            if(inHand != null && inHand.isSimilar(ITEM) && event.getPlayer() == player){
                //CheckWorld
                if(!checkWorld(event.getPlayer().getWorld() , selector)){
                    event.getPlayer().sendMessage(bundle.getString("selectorErrorWorld"));
                    return;
                }
                //Check Click
                switch (event.getAction()){
                    case LEFT_CLICK_BLOCK:
                        selector.setFirstLocation(getCenter(event.getClickedBlock().getLocation()));
                        event.getPlayer().sendMessage(Variables.replaceLocation(
                                ChatColor.translateAlternateColorCodes('&',bundle.getString("selectorFirstLocation")),
                                selector.getFirstLocation()));
                        break;
                    case RIGHT_CLICK_BLOCK:
                        selector.setSecondLocation(getCenter(event.getClickedBlock().getLocation()));
                        event.getPlayer().sendMessage(Variables.replaceLocation(
                                ChatColor.translateAlternateColorCodes('&',bundle.getString("selectorSecondLocation")), selector.getSecondLocation()));
                        break;
                    default:
                        break;
                }
            }
        }

        /**
         * Check world
         * @param world world to be checked
         * @param selector selector to be checked
         * @return true if the selector world equals to world
         */
        public static boolean checkWorld(World world , Selector selector){
            boolean valid = true;
            if(selector.getFirstLocation() != null)
                valid = selector.getFirstLocation().getWorld() == world;
            if(selector.getSecondLocation() != null)
                valid &= selector.getSecondLocation().getWorld() == world;
            return valid;
        }

        public static Location getCenter(Location location){
            return new Location(location.getWorld() , location.getBlockX() + 0.5 , location.getY() , location.getBlockZ() + 0.5);
        }

        /**
         * On player quit event
         * @param event player quit event
         */
        @EventHandler
        public void onPlayerQuit(PlayerQuitEvent event){
            instances.remove(event.getPlayer());
        }
    }

    @FunctionalInterface
    public interface SelectorAdd{
        /**
         * On selector added to player
         * @param player player
         * @param s selector
         */
        void onAdd(Player player , Selector s);
    }


    /**
     * Get selector for this player
     * @param p player to get
     * @param create create if not exist
     * @return Selector for this player
     */
    public static Selector get(Player p , boolean create){
        if(create && !instances.containsKey(p)){
            Selector selector = new Selector(p);
            instances.put(p , selector);
            listeners.forEach(l -> l.onAdd(p , selector));
            return selector;
        }
        else
            return instances.get(p);
    }

    /**
     * Add selector listener
     * @param listener listener to be add
     */
    public static void addListener(SelectorAdd listener){
        listeners.add(listener);
    }
}
