package xyz.deltaevo.villagercolor.i18n;

import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Locale;
import java.util.ResourceBundle;

//Thanks SonarLint x)

public class BundleManager {
    //Class Loader with url folder
    private final URLClassLoader loader;
    //Bundle Base Name
    private final String baseName;

    /**
     * Create new BundleManager , it use baseName as bundle BaseName
     * and search bundles in folders'urls specified
     * @param baseName , Bundle Base Name
     * @param folders , Folders to search bundle urls
     */
    public BundleManager(String baseName , URL...folders){
        this.baseName = baseName;
        this.loader = new URLClassLoader(folders);
    }

    /**
     * Get Bundle for Locale
     * @param locale
     * @return ResourceBundle for the Locale
     * @see ResourceBundle#getBundle(String, Locale)
     */
    public ResourceBundle getBundle(Locale locale){
        return ResourceBundle.getBundle(baseName , locale , loader);
    }

    public void close() throws IOException {
        loader.close();
    }
}
