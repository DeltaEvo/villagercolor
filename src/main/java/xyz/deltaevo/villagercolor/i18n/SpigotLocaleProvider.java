package xyz.deltaevo.villagercolor.i18n;

import org.bukkit.entity.Player;

import java.util.Locale;

/**
 * Locale Provider using Spigot Methods
 */
public class SpigotLocaleProvider implements LocaleProvider{
    @Override
    public Locale getLocale(Player p) {
        return new Locale(p.spigot().getLocale());
    }
}
