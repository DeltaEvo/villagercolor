package xyz.deltaevo.villagercolor.i18n;

import org.bukkit.Location;

public class Variables {
    public static final String NAME = "{name}";
    public static final String X = "{x}";
    public static final String Y = "{y}";
    public static final String Z = "{z}";
    public static final String WORLD = "{world}";

    public static final String PATH_LOCATION_FORMAT = "locationFormat";
    public static final String PATH_UNKNOWN_ARENA = "unknownArena";

    /**
     * DON'T USE THIS CONSTRUCTOR
     * @throws IllegalAccessException because there is no instance for you :p
     */
    private Variables() throws IllegalAccessException {
        throw new IllegalAccessException("No instance for you");
    }

    /**
     * Replace Location in this phrase
     * @param phrase phrase to replace location
     * @param location location to be replaced
     * @return phrase with replaced location
     */
    public static String replaceLocation(String phrase , Location location){
        if(location == null)
            return "";
        else
            return phrase.replace(X, Integer.toString(location.getBlockX()))
                .replace(Y , Integer.toString(location.getBlockY()))
                .replace(Z , Integer.toString(location.getBlockZ()))
                .replace(WORLD , location.getWorld().getName());
    }
}
