package xyz.deltaevo.villagercolor.i18n;

import org.bukkit.entity.Player;

import java.util.Locale;

@FunctionalInterface
public interface LocaleProvider {
    /**
     * Get locale for this player
     * @param p this player
     * @return the locale
     */
    Locale getLocale(Player p);
}
