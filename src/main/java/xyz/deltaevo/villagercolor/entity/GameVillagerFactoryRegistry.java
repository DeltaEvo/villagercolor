package xyz.deltaevo.villagercolor.entity;

import org.bukkit.Location;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * Class used to register all GameVillager creator
 */
public class GameVillagerFactoryRegistry {
    private Map<String , Function<Location, GameVillager>> creators;

    public GameVillagerFactoryRegistry() {
        this.creators = new HashMap<>();
    }

    /**
     * Register new Game Villager Creator
     * @param name name of the creator
     * @param function creator
     */
    public void register(String name , Function<Location , GameVillager> function){
        if(creators.containsKey(name))
            throw new IllegalArgumentException("Name : " +name+ " already used");
        else creators.put(name.toUpperCase() , function);
    }

    /**
     * Get creator from name
     * @param name name to be searched
     * @return creator if find or null
     */
    public Function<Location , GameVillager> getFunction(String name){
        return creators.get(name.toUpperCase());
    }
}
