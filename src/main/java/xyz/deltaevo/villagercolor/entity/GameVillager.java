package xyz.deltaevo.villagercolor.entity;

import org.bukkit.Location;

public interface GameVillager {
    /**
     * Go to specified Location with specified speed
     * @param loc location to go to
     * @param speed speed to use
     */
    void goTo(Location loc , double speed);

    /**
     * Set current Villager Color
     * @param color VillagerColor
     */
    void setColor(GameVillagerColor color);

    /**
     * Get current Villager Color
     * @return  color VillagerColor
     */
    GameVillagerColor getColor();

    /**
     * Get current location
     * @return location
     */
    Location getLocation();

    /**
     * Remove current villager
     */
    void remove();
}
