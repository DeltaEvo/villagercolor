package xyz.deltaevo.villagercolor.entity.nms;

import org.bukkit.Bukkit;
import xyz.deltaevo.villagercolor.VillagerColor;
import xyz.deltaevo.villagercolor.java.reflection.ParamClass;
import xyz.deltaevo.villagercolor.java.reflection.Reflection;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.Map;
import java.util.logging.Level;

public class EntityTypesRegistry {

    public static final ParamClass<Map<Class<?> , String>> CLASS_TO_NAME_TYPE =
            new ParamClass<Map<Class<?>, String>>() {};
    public static final ParamClass<Map<Class<?> , Integer>> CLASS_TO_ID_TYPE =
            new ParamClass<Map<Class<?>, Integer>>() {};
    public static final ParamClass<Map<String , Class<?>>> NAME_TO_CLASS_TYPE =
            new ParamClass<Map<String, Class<?>>>() {};
    public static final ParamClass<Map<Integer , Class<?>>> ID_TO_CLASS_TYPE =
            new ParamClass<Map<Integer , Class<?>>>() {};
    public static final ParamClass<Map<String , Integer>> NAME_TO_ID_TYPE =
            new ParamClass<Map<String , Integer>>() {};


    private static Map<Class<?> , String> classToName;
    private static Map<Class<?> , Integer> classToId;
    private static Map<String , Class<?>> nameToClass;
    private static Map<Integer , Class<?>> idToClass;
    private static Map<String , Integer> nameToId;

    static {
        String nmsver = Bukkit.getServer().getClass().getPackage().getName();
        nmsver = nmsver.substring(nmsver.lastIndexOf(".") + 1);
        try {
            for(Field f : Class.forName(VillagerColor.NMS_PACKAGE + "." + nmsver + ".EntityTypes").getDeclaredFields()){
                f.setAccessible(true);
                if(!(f.getGenericType() instanceof ParameterizedType))
                    continue;

                //Get fields instances
                if(compareTypeArguments((ParameterizedType)f.getGenericType() ,
                        (ParameterizedType)CLASS_TO_NAME_TYPE.getType()))
                    classToName = CLASS_TO_NAME_TYPE.getClazz().cast(f.get(null));

                else if(compareTypeArguments((ParameterizedType)f.getGenericType() ,
                        (ParameterizedType)CLASS_TO_ID_TYPE.getType()))
                    classToId = CLASS_TO_ID_TYPE.getClazz().cast(f.get(null));

                else if(compareTypeArguments((ParameterizedType)f.getGenericType() ,
                        (ParameterizedType)NAME_TO_CLASS_TYPE.getType()))
                    nameToClass = NAME_TO_CLASS_TYPE.getClazz().cast(f.get(null));

                else if(compareTypeArguments((ParameterizedType)f.getGenericType() ,
                        (ParameterizedType)ID_TO_CLASS_TYPE.getType()))
                    idToClass = ID_TO_CLASS_TYPE.getClazz().cast(f.get(null));

                else if(compareTypeArguments((ParameterizedType)f.getGenericType() ,
                        (ParameterizedType)NAME_TO_ID_TYPE.getType()))
                    nameToId = NAME_TO_ID_TYPE.getClazz().cast(f.get(null));

            }
        } catch (ReflectiveOperationException e) {
            VillagerColor.logger.log(Level.WARNING , "Could not find EntityTypes or acces to this fieldsclass are you using NMS ?" , e);
        }
    }

    /**
     * DON'T USE THIS CONSTRUCTOR
     * @throws IllegalAccessException because there is no instance for you :p
     */
    private EntityTypesRegistry() throws IllegalAccessException {
        throw new IllegalAccessException("No instance for you");
    }

    private static boolean compareTypeArguments(ParameterizedType type , ParameterizedType type2){
        return Reflection.compare(type.getActualTypeArguments() , type2.getActualTypeArguments());
    }


    /**
     * Register entity in EntityTypes
     * @param entityClass class extending Entity
     * @param name name of the entity
     * @param id id of the entity
     */
    public static void register(Class<?> entityClass , String name , int id){
        classToName.put(entityClass , name);
        classToId.put(entityClass , id);
        nameToClass.put(name , entityClass);
        nameToId.put(name , id);
    }

    /**
     * Add temporally id to EntityTypes
     * @param entity entity class (must be registered)
     * @param runnable action to execute when class is registered
     * @param <T> return type
     * @return runnable return
     */
    public static <T> T spawn(Class<?> entity , RegistryRunnable<T> runnable){
        if(!classToId.containsKey(entity))
            throw new IllegalStateException("Entity " + entity + " not registered");
        int id = classToId.get(entity);
        //Set custom Entity
        Class<?> oldClass = idToClass.put(id , entity);
        T result = runnable.run(); //Register
        //Reset old Entity
        idToClass.put(id , oldClass);
        return result;
    }

    @FunctionalInterface
    public interface RegistryRunnable<T>{
        /**
         * Run action
         * @return what you want
         */
        T run();
    }

}
