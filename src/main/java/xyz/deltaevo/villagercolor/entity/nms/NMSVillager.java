package xyz.deltaevo.villagercolor.entity.nms;

import net.minecraft.server.v1_8_R3.*;

import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.entity.EntityType;

import xyz.deltaevo.villagercolor.entity.GameVillagerColor;
import xyz.deltaevo.villagercolor.VillagerColor;
import xyz.deltaevo.villagercolor.entity.GameVillager;

import java.util.logging.Level;

public class NMSVillager extends EntityCreature implements GameVillager , IAnimal{
    //Register Class to EntityType to not override default villager
    static {
        EntityTypesRegistry.register(NMSVillager.class , "NMSVillager" , EntityType.VILLAGER.getTypeId());
    }

    /**
     * Create new NMSVillager in the specified NMS World
     * @param world nms World
     */
    protected NMSVillager(World world) {
        super(world);
        this.persistent = true;
        setInvisible(false);
    }

    /**
     * Spawn new NMSVillager to te specified location
     * @param loc location to spawn Villager
     */
    public static NMSVillager spawn(Location loc){
        return EntityTypesRegistry.spawn(NMSVillager.class , () -> {
            NMSVillager villager = new NMSVillager(((CraftWorld) loc.getWorld()).getHandle());
            villager.setPosition(loc.getX(), loc.getY(), loc.getZ());
            if (!villager.getWorld().addEntity(villager))
                VillagerColor.logger.log(Level.INFO, "Entity " + villager + " could not be added to the world");
            villager.initDataWatcher();
            return villager;
        });
    }

    /**
     * Init DataWatcher
     */
    protected void initDataWatcher(){
        getDataWatcher().a(16, Integer.valueOf(0));
        getDataWatcher().a(12, Byte.valueOf((byte) 0));
    }

    @Override
    public void goTo(Location loc , double speed) {
        this.getNavigation().a(loc.getX() , loc.getY() , loc.getZ() , speed);
    }

    @Override
    public void setColor(GameVillagerColor color) {
        getDataWatcher().watch(16 , Integer.valueOf(color.getId()));
    }

    @Override
    public GameVillagerColor getColor() {
        return GameVillagerColor.valueOf(getDataWatcher().getInt(16));
    }

    @Override
    public Location getLocation() {
        return getBukkitEntity().getLocation();
    }

    @Override
    public void remove() {
        getBukkitEntity().remove();
    }

    @Override
    protected String z() {
        return "mob.villager.idle";
    }

    @Override
    protected String bo() {
        return "mob.villager.hit";
    }

    @Override
    protected String bp() {
        return "mob.villager.death";
    }

}
