package xyz.deltaevo.villagercolor.entity;


import org.bukkit.Location;

import java.util.function.Function;

public class GameVillagerFactory {
    //Constructor function
    private final Function<Location, GameVillager> creator;

    /**
     * Create new Villager Factory with method , usually a constructor like Class::new
     * @param creator function to create GameVillager
     */
    public GameVillagerFactory(Function<Location, GameVillager> creator){
        this.creator = creator;
    }


    /**
     * Create new GameVillager at specified location
     * @param loc location to spawn Villager
     * @return Instance of GameVillager created
     */
    public GameVillager create(Location loc){
        return creator.apply(loc);
    }
}
