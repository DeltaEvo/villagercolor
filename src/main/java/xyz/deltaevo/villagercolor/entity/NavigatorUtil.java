package xyz.deltaevo.villagercolor.entity;

import org.bukkit.Bukkit;
import org.bukkit.entity.Creature;
import xyz.deltaevo.villagercolor.VillagerColor;

import java.lang.reflect.Method;
import java.util.function.BiConsumer;
import java.util.logging.Level;

public class NavigatorUtil {
    /*
        EntityType.register Method (a in obfuscated code generally)
     */
    private static Method register;

    /*
        Method to set target of the mob
     */
    private static BiConsumer<Creature , double[]> setTarget;

    //Init static fields
    static {
        String nmsver = Bukkit.getServer().getClass().getPackage().getName();
        nmsver = nmsver.substring(nmsver.lastIndexOf(".") + 1);
        try {
            //Get register(a) method from EntityTypes using Field Types
            for(Method m : Class.forName(VillagerColor.NMS_PACKAGE + "." + nmsver + ".EntityTypes").getDeclaredMethods()){
                if(m.getParameterCount() == 3 &&
                        m.getParameterTypes()[0].isAssignableFrom(Class.class) &&
                        m.getParameterTypes()[1].isAssignableFrom(String.class) &&
                        m.getParameterTypes()[2].isAssignableFrom(int.class)){
                    register = m;
                    break;
                }
            }
            //Prepare NMS Classes and Methods
            Class<?> craftCreature = Class.forName(VillagerColor.CRAFTBUKKIT_PACKAGE+"."+nmsver+".entity.CraftCreature");
            Method getHandle = craftCreature.getDeclaredMethod("getHandle");
            Class<?> nmsLivingEntity = Class.forName(VillagerColor.NMS_PACKAGE + "."+nmsver+".EntityInsentient");
            Method getNavigation = nmsLivingEntity.getDeclaredMethod("getNavigation");
            Class<?> navigationAbstract = Class.forName(VillagerColor.NMS_PACKAGE + "."+nmsver+".NavigationAbstract");
            Method a = navigationAbstract.getMethod("a" , double.class , double.class , double.class , double.class);
            setTarget = (entity , params) -> setTarget(entity , params , getHandle , getNavigation , a);
        } catch (ReflectiveOperationException e) {
            VillagerColor.logger.log(Level.WARNING , "Could not find class are you using NMS ?" , e);
        }

    }

    /**
     * DON'T USE THIS CONSTRUCTOR
     * @throws IllegalAccessException because there is no instance for you :p
     */
    private NavigatorUtil() throws IllegalAccessException { throw new IllegalAccessException("No instance for you");}

    public static void register(Class<?> entityClass , String name , int id) throws ReflectiveOperationException {
       register.invoke(null , entityClass , name , id);
    }

    /**
     * Set entity target using nms method
     * @param entity entity to set target
     * @param x x target coordinate
     * @param y y target coordinate
     * @param z z target coordinate
     * @param speed mob speed
     */
    public static void setTarget(Creature entity , double x, double y , double z , double speed){
        setTarget.accept(entity , new double[]{x , y ,z , speed});
    }

    /**
     * Create Target method from NMS Classes
     * @param entity entity to set target
     * @param params params of the method (3 params)
     * @param getHandle Creature.getHandle CraftBukkit method
     * @param getNavigation EntityInsentient.getNavigation
     * @param a AbstractNaviagtion.a method
     */
    private static void setTarget(Creature entity , double[] params , Method getHandle , Method getNavigation , Method a){
        try {
            a.invoke(getNavigation.invoke(getHandle.invoke(entity)) , params[0] , params[1] , params[2] , params[3]);

        } catch (ReflectiveOperationException e) {
            VillagerColor.logger.log(Level.SEVERE , "Could not set Target" , e);
        }
    }
}
