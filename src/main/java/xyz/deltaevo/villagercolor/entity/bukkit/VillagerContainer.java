package xyz.deltaevo.villagercolor.entity.bukkit;

import org.bukkit.Location;
import org.bukkit.entity.Villager;

import xyz.deltaevo.villagercolor.entity.GameVillagerColor;
import xyz.deltaevo.villagercolor.entity.NavigatorUtil;
import xyz.deltaevo.villagercolor.entity.GameVillager;

public class VillagerContainer implements GameVillager{
    //Bukkit Villager Instance
    private Villager villager;

    /**
     * Create a new Bukkit Villager Owner by this container at Location loc
     * @param loc Location to spawn the Villager
     */
    public VillagerContainer(Location loc){
        this.villager = loc.getWorld().spawn(loc , Villager.class);
    }

    /**
     * Get the Bukkit Villager owned by this container
     * @return the Bukkit Villager
     */
    public Villager getVillager() {
        return villager;
    }

    @Override
    public void goTo(Location loc , double speed) {
        NavigatorUtil.setTarget(villager , loc.getX() , loc.getY() , loc.getZ() , speed);
    }

    @Override
    public void setColor(GameVillagerColor color) {
        villager.setProfession(color.getProfession());
    }

    @Override
    public GameVillagerColor getColor() {
        return GameVillagerColor.valueOf(villager.getProfession().getId());
    }

    @Override
    public Location getLocation() {
        return villager.getLocation();
    }

    @Override
    public void remove() {
        villager.remove();
    }
}
