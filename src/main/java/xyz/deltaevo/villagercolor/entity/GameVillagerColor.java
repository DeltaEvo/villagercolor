package xyz.deltaevo.villagercolor.entity;

import org.bukkit.entity.Villager;

public enum GameVillagerColor {
    BROWN(0),
    WHITE(1),
    PURPLE(2),
    BLACK_TABLIER(3),
    WHITE_TABLIER(4);

    private int id;

    GameVillagerColor(int id) {
        this.id = id;
    }

    /**
     * Get profession id of this color
     * @return profession id of the color
     */
    public int getId() {
        return this.id;
    }

    /**
     * Get bukkit profession
     * @return bukkit profesion
     */
    public Villager.Profession getProfession(){
        return Villager.Profession.getProfession(getId());
    }

    public static GameVillagerColor valueOf(int id) {
        for(GameVillagerColor color : values())
            if(color.getId() == id)
                return color;
        return null;
    }
}
