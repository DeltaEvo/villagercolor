package xyz.deltaevo.villagercolor.gson;

import com.google.gson.*;
import xyz.deltaevo.villagercolor.VillagerColor;

import java.lang.reflect.Type;
import java.util.logging.Level;

public class InterfaceAdapter<T> implements JsonSerializer<T> , JsonDeserializer<T> {
    @Override
    public T deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) {
        try {
            JsonObject object = (JsonObject) jsonElement;
            Class<?> t = Class.forName(object.get("type").getAsString());
            //Primitive
            if(object.has("data")){
                return jsonDeserializationContext.deserialize(object.get("data") , t);
            }
            //Object
            else{
                object.remove("type");
                return jsonDeserializationContext.deserialize(object , t);
            }
        } catch (ClassNotFoundException e) {
            VillagerColor.logger.log(Level.WARNING , "Unknown class" , e);
            return null;
        }
    }

    @Override
    public JsonElement serialize(T t, Type type, JsonSerializationContext jsonSerializationContext) {
        JsonElement element = jsonSerializationContext.serialize(t);
        if(element.isJsonObject()){
            JsonObject object = (JsonObject) element;
            object.add("type" , new JsonPrimitive(t.getClass().getName()));
            return object;
        }else {
            JsonObject object = new JsonObject();
            object.add("type" , new JsonPrimitive(t.getClass().getName()));
            object.add("data" , element);
            return object;
        }
    }
}
