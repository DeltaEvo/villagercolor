package xyz.deltaevo.villagercolor.gson;

import com.google.gson.*;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import xyz.deltaevo.villagercolor.java.reflection.ParamClass;
import xyz.deltaevo.villagercolor.java.reflection.Reflection;

import java.lang.reflect.Type;
import java.util.HashMap;

public class ConfigurationSerializableAdapter implements JsonSerializer<ConfigurationSerializable>,
        JsonDeserializer<ConfigurationSerializable>{

    public static final ConfigurationSerializableAdapter INSTANCE = new ConfigurationSerializableAdapter();

    private ConfigurationSerializableAdapter() {}

    @Override
    public ConfigurationSerializable deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext){
        return ConfigurationSerialization.deserializeObject(jsonDeserializationContext.deserialize(jsonElement , new ParamClass<HashMap<String , ?>>(){}.getType()) , Reflection.<ConfigurationSerializable>getClazz(type));
    }

    @Override
    public JsonElement serialize(ConfigurationSerializable configurationSerializable, Type type, JsonSerializationContext jsonSerializationContext) {
        return jsonSerializationContext.serialize(configurationSerializable.serialize());
    }
}
