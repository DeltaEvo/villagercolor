package xyz.deltaevo.villagercolor.area;

import org.bukkit.Location;
import org.bukkit.entity.Entity;

public interface Area {

    /**
     * Check if this is in the area
     * @param world , the word's name to check
     * @return true if is in area
     */
    boolean worldIsIn(String world);

    /**
     * Check if this is in the area
     * @param x , the x coordinate to check
     * @return true if is in area
     */
    boolean xIsIn(int x);

    /**
     * Check if this is in the area
     * @param y , the y coordinate to check
     * @return true if is in area
     */
    boolean yIsIn(int y);

    /**
     * Check if this is in the area
     * @param z , the z coordinate to check
     * @return true if is in area
     */
    boolean zIsIn(int z);

    /**
     * Get Area world name
     * @return world name
     */
    String getWorld();

    /**
     * Check if this is in the area
     * @param world , the word's name to check
     * @param x , the x coordinate to check
     * @param y , the y coordinate to check
     * @param z , the z coordinate to check
     * @return true if is in area
     */
    default boolean isIn(String world , int x , int y , int z){
        return worldIsIn(world) && xIsIn(x) && yIsIn(y) && zIsIn(z);
    }

    /**
     * Check if the location is in Area
     * @param loc , the location to check
     * @return true if is in area
     */
    default boolean isIn(Location loc){
        return isIn(loc.getWorld().getName() , loc.getBlockX() , loc.getBlockY() , loc.getBlockZ());
    }

    /**
     * Check if entity is in Area
     * @param ent , the entity to check
     * @return true if is in area
     */
    default boolean isIn(Entity ent){
        return isIn(ent.getLocation());
    }
}
