package xyz.deltaevo.villagercolor.area;

import org.bukkit.event.Listener;
import xyz.deltaevo.villagercolor.VillagerColor;
import xyz.deltaevo.villagercolor.command.CommandListener;

import java.util.ResourceBundle;

public interface AreaCreator extends Listener{
    /**
     * Check if the area is valid and can be used
     * @return if the Area is valid
     */
    boolean isValid();

    /**
     * Get error message if is not valid
     * @param lang ResourceBundle to get Message
     * @return error message
     */
    String nonValidMessage(ResourceBundle lang);

    /**
     * Get area of this creator
     * @return the area of this creator
     */
    Area getArea();

    interface AreaCreatorCommand extends CommandListener{
        /**
         * Init Area Creator Command
         * @param area Player Area managing this command
         * @param instance VillagerColor instance
         */
        void setPlayerArea(PlayerArea area , VillagerColor instance);

        /**
         * Get help for this command
         * @param lang ResourceBundle to get Message
         * @return help message
         */
        String getHelp(ResourceBundle lang);
    }
}
