package xyz.deltaevo.villagercolor.area.cuboid;

import xyz.deltaevo.villagercolor.area.Area;

public class CuboidArea implements Area {
    private String world;
    private int minX;
    private int minY;
    private int minZ;
    private int maxX;
    private int maxY;
    private int maxZ;

    /**
     * Create new Cuboid Area with specified locations and World
     * @param world world of the area
     * @param x1 first x coordinate of the area
     * @param y1 first y coordinate of the area
     * @param z1 first z coordinate of the area
     * @param x2 second z coordinate of the area
     * @param y2 second y coordinate of the area
     * @param z2 second z coordinate of the area
     */
    public CuboidArea(String world, int x1, int y1, int z1, int x2, int y2, int z2) {
        if(world == null)
            throw new IllegalArgumentException("world can't be null");
        this.world = world;
        this.minX = Math.min(x1 , x2);
        this.minY = Math.min(y1 , y2);
        this.minZ = Math.min(z1 , z2);
        this.maxX = Math.max(x1 , x2);
        this.maxY = Math.max(y1 , y2);
        this.maxZ = Math.max(z1 , z2);
    }

    @Override
    public boolean worldIsIn(String world) {
        return this.world.equals(world);
    }

    @Override
    public boolean xIsIn(int x) {
        return maxX >= x && minX <= x ;
    }

    @Override
    public boolean yIsIn(int y) {
        return maxY >= y && minY <= y;
    }

    @Override
    public boolean zIsIn(int z) {
        return maxZ >= z && minZ <= z;
    }

    @Override
    public String getWorld() {
        return world;
    }
}
