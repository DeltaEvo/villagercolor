package xyz.deltaevo.villagercolor.area.cuboid;

import org.bukkit.Location;

import xyz.deltaevo.villagercolor.VillagerColor;
import xyz.deltaevo.villagercolor.area.Area;
import xyz.deltaevo.villagercolor.area.AreaCreator;
import xyz.deltaevo.villagercolor.area.PlayerArea;
import xyz.deltaevo.villagercolor.selector.Selector;

import java.util.ResourceBundle;

public class CuboidAreaCreator implements AreaCreator{

    private final Selector selector;

    /**
     * Create new CuboidAreaCreator with specified Selector
     * @param selector selector to create Area
     * @see CuboidArea
     */
    public CuboidAreaCreator(Selector selector) {
        this.selector = selector;
    }


    @Override
    public boolean isValid() {
        return selector.hasTwoLocations();
    }

    @Override
    public String nonValidMessage(ResourceBundle lang) {
        return selector.getHelp(lang);
    }

    @Override
    public Area getArea() {
        if(isValid()){
            Location loc1 = selector.getFirstLocation();
            Location loc2 = selector.getSecondLocation();
            return new CuboidArea(loc1.getWorld().getName() ,
                    loc1.getBlockX() , loc1.getBlockY() , loc1.getBlockZ() ,
                    loc2.getBlockX() , loc2.getBlockY() , loc2.getBlockZ());
        } else
            return null;
    }

    public static class CuboidAreaCreatorCommand implements AreaCreatorCommand{

        @Override
        public void setPlayerArea(final PlayerArea area , VillagerColor instance) {
            Selector.addListener((p , s) -> area.setCreator(p , new CuboidAreaCreator(s)));
        }

        @Override
        public String getHelp(ResourceBundle lang) {
            return lang.getString("cuboidAreaHelp");
        }

    }
}
