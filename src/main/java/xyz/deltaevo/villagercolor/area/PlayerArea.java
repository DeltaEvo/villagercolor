package xyz.deltaevo.villagercolor.area;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import xyz.deltaevo.villagercolor.VillagerColor;
import xyz.deltaevo.villagercolor.command.CommandRegisterer;

import java.util.*;
import java.util.stream.Collectors;

public class PlayerArea implements Listener{
    private final CommandRegisterer commandRegisterer;
    private final VillagerColor instance;
    private final Map<Player , AreaCreator> creators;
    private final List<AreaCreator.AreaCreatorCommand> commands;

    /**
     * Create new PlayerAreaManager using provided commandRegisterer and Villager Color instance
     * @param commandRegisterer commandRegisterer to register AreaCreatorCommand commands
     * @param instance VillagerColor instance
     */
    public PlayerArea(CommandRegisterer commandRegisterer , VillagerColor instance) {
        this.commandRegisterer = commandRegisterer;
        this.instance = instance;
        this.creators = new HashMap<>();
        this.commands = new ArrayList<>();
    }


    /**
     * Register new Area Creator command
     * @param command command to register
     */
    public void registerCreatorCommand(AreaCreator.AreaCreatorCommand command){
        commands.add(command);
        commandRegisterer.getListeners().add(command);
        command.setPlayerArea(this , instance);
    }

    /**
     * Set player Creator
     * @param p player to set creator
     * @param creator creator
     */
    public void setCreator(Player p , AreaCreator creator) {
        this.creators.put(p , creator);
        instance.getServer().getPluginManager().registerEvents(creator , instance);
    }

    /**
     * Remove player creator
     * @param p player creator
     */
    public void removeCreator(Player p) {
        if(this.creators.containsKey(p))
            HandlerList.unregisterAll(this.creators.remove(p));
    }

    /**
     * Get Help to create Area
     * @param bundle bundle to get lang messages
     * @return help
     */
    public String getHelp(ResourceBundle bundle){
        return commands.stream().map(c -> c.getHelp(bundle)).collect(Collectors.joining("\n"));
    }

    /**
     * Get AreaCreator for player
     * @param p player to get area creator
     * @return player's AreaCreator or null
     */
    public AreaCreator getCreator(Player p){
        return this.creators.get(p);
    }

    /**
     * Get an unmodifiable map of players creators
     * @return unmodifiable map of players creators
     */
    public Map<Player, AreaCreator> getCreators() {
        return Collections.unmodifiableMap(creators);
    }

    /**
     * On player quit game
     * @param event bukkit event
     */
    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event){
        removeCreator(event.getPlayer());
    }
}
